-- 
-- Βάση: `ergasthrio_ab`
-- 

DROP DATABASE IF EXISTS `ergasthrio_ab`;
CREATE DATABASE `ergasthrio_ab` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;

USE `ergasthrio_ab`;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `activities`
-- 

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(3) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `activity_table` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `activities`
-- 

INSERT INTO `activities` VALUES (1, 'Οι συλλαβές μπερδεύτηκαν', 'syllabes_mperdeuthkan');
INSERT INTO `activities` VALUES (2, 'Φτιάχνω τις λέξεις', 'ftiaxno_lekseis');
INSERT INTO `activities` VALUES (3, 'Ταξινόμηση', 'taksinomisi');
INSERT INTO `activities` VALUES (4, 'Οι προτάσεις μπερδεύτηκαν', 'protaseis_mperdeuthkan');
INSERT INTO `activities` VALUES (5, 'Τα μπαλόνια', 'mpalonia');
INSERT INTO `activities` VALUES (6, 'Βρίσκω τις συλλαβές', 'brisko_syllabes');
INSERT INTO `activities` VALUES (7, 'Ακροστοιχίδα', 'akrostoixida');
INSERT INTO `activities` VALUES (8, 'Εικόνες', 'eikones');
INSERT INTO `activities` VALUES (9, 'Μαϊμού', 'maimou');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `akrostoixida`
-- 

DROP TABLE IF EXISTS `akrostoixida`;
CREATE TABLE IF NOT EXISTS `akrostoixida` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `data_text` text NOT NULL,
  `data_images` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `akrostoixida`
-- 

INSERT INTO `akrostoixida` VALUES (1, 1, 'Αλοιφή\r\nΝοικοκυρά\r\nΟδηγοί\r\nΙνδιάνοι\r\nΞιφομάχος\r\nΗθοποιός', '1_aloifh.jpg\r\n1_noikokoira.jpg\r\n1_odhgoi.jpg\r\n1_indianoi.jpg\r\n1_ksifomaxos.jpg\r\n1_hthopoios.jpg', '1_final.jpg');
INSERT INTO `akrostoixida` VALUES (4, 3, 'Κάκτοι\r\nΟδηγοί\r\nΙνδιάνοι\r\nΛαγοί\r\nΙατροί\r\nΆνοιξη', '3_kaktoi.jpg\r\n3_odigoi.jpg\r\n3_indianoi.jpg\r\n3_lagoi.jpg\r\n3_iatroi.jpg\r\n3_anoiksi.jpg', '3_koilia.jpg');
INSERT INTO `akrostoixida` VALUES (5, 10, 'Κοριτσάκι\r\nΟμπρέλα\r\nΥπομονή\r\nΠίνακας\r\nΙππόκαμπος', '', '10_5peti.jpg');
INSERT INTO `akrostoixida` VALUES (6, 10, 'Κουτάλι\r\nΡινόκερος\r\nΥπομονή\r\nΟλυμπία', '', '10_8peti.jpg');
INSERT INTO `akrostoixida` VALUES (7, 18, 'Κάκτοι\r\nΟδηγοί\r\nΙνδιάνοι\r\nΚαράβι\r\nΑεροπλάνο', '18_Ledger.gif\r\n18_Assets.gif\r\n18_Timebill.gif\r\n18_Service.gif\r\n18_Resource.gif', '18_Assets.gif');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `brisko_syllabes`
-- 

DROP TABLE IF EXISTS `brisko_syllabes`;
CREATE TABLE IF NOT EXISTS `brisko_syllabes` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `word_without` varchar(255) NOT NULL default '',
  `syllables` varchar(255) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `brisko_syllabes`
-- 

INSERT INTO `brisko_syllabes` VALUES (1, 1, 'Η _ρανία είναι ό_ρφη', 'Ου;μο', '1_ourania.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (3, 1, 'Η Κοκκινο_ουφί_α ζούσε σε ένα _ιτάκι κο_ά στο κα_αναριό', 'σκ;τσ;σπ;ντ;μπ', '1_kokki.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (7, 1, 'Ο _α_άς αγόρασε κόκκινες _ομάτες', 'μπ;μπ;ντ', '1_ntomates.jpg', 0);
INSERT INTO `brisko_syllabes` VALUES (8, 1, 'Τα _οτάκια της Μαρίας είναι κί_ινα', 'μπ;τρ', '1_mpotes.jpg', 0);
INSERT INTO `brisko_syllabes` VALUES (9, 3, 'Ήταν ένα κορι_άκι που φορ_σε πά_α ένα κόκκινο _ουφάκι.', 'τσ;ού;ντ;σκ', '3_kokkino.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (17, 6, 'Είναι μια δο_μή', 'κι', '6_τεστ3.JPG', 1);
INSERT INTO `brisko_syllabes` VALUES (18, 11, 'κό_ και _κρά αυ_', 'τα;μι;γά', '', 1);
INSERT INTO `brisko_syllabes` VALUES (19, 10, 'το _άκι στο _ιτάκι του χωριού ήταν αναμένο και ο κα_ός έβγαινε από την καμινάδα', 'τζ;σπ;πν;', '', 1);
INSERT INTO `brisko_syllabes` VALUES (20, 10, 'η α_ξη έ_εται κάθε _νο μετά το _μώνα και μας δίνει χαρά', 'νοι;χρ;χρό;ει', '10_5peti.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (21, 10, 'λά_ ο ήλιος στο _νό και στους κά_', 'μπει;βου;μπους', '10_8peti.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (22, 13, 'Η ά_ξη μας ήρθε γε_τη με λου_δια', 'νοι;μά;λού', '13_8peti.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (23, 13, 'Το μι_ σπι_κι ήταν γε_το γέ_', 'κρό;τά;μά;λια', '13_5peti.jpg', 1);
INSERT INTO `brisko_syllabes` VALUES (24, 18, 'Ήταν; ένα κορι__άκι', 'τσ;', '18_preview20AK35.jpg', 1);

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `eikones`
-- 

DROP TABLE IF EXISTS `eikones`;
CREATE TABLE IF NOT EXISTS `eikones` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `story` int(3) NOT NULL default '0',
  `word` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `eikones`
-- 

INSERT INTO `eikones` VALUES (1, 1, 1, 'Ένας κάβουρας βγήκε απο την θάλασσα\r\nκαι ζούσε μόνος του στην αμμουδιά', '1_kabouras1.jpg');
INSERT INTO `eikones` VALUES (2, 1, 1, 'Τον είδε μια αλεπού και αφού δεν είχε τίποτα άλλο να φάει,\r\nόρμηξε και τον άρπαξε', '1_kabouras2.jpg');
INSERT INTO `eikones` VALUES (3, 1, 1, '«Καλά να πάθω», φώναξε ο κάβουρας.\r\n«Αφού είμαι θαλασσινός τι μ'' έπιασε και θέλησα να γίνω στεριανός;»', '1_kabouras3.jpg');
INSERT INTO `eikones` VALUES (6, 3, 1, 'Ο λύκος έφτασε πρώτος στη γιαγιά.', '3_lykos1.jpg');
INSERT INTO `eikones` VALUES (7, 3, 1, 'Έκλεισε τη γιαγιά στη ντουλάπα.', '3_lykos2.jpg');
INSERT INTO `eikones` VALUES (8, 3, 1, 'Μόλις έφθασε το κοριτσάκι το έφαγε.', '3_lykos3.jpg');
INSERT INTO `eikones` VALUES (14, 3, 1, 'Ξάπλωσε σε ένα δέντρο και κοιμήθηκε.', '3_lykos4.jpg');
INSERT INTO `eikones` VALUES (15, 6, 1, 'Αυτή είναι μια ακόμη δοκιμή για εικόνες', '6_3.JPG');
INSERT INTO `eikones` VALUES (16, 10, 1, 'η έρημος της Σαχάρας έχει άμμο χρυσαφί', '10_5peti.jpg');
INSERT INTO `eikones` VALUES (17, 10, 1, 'Οι βεδουίνοι ταξιδεύουν με τις γκαμήλες τους', '10_8peti.jpg');
INSERT INTO `eikones` VALUES (18, 10, 2, 'έρημος', '10_5peti.jpg');
INSERT INTO `eikones` VALUES (19, 10, 2, 'βεδουίνος', '10_8peti.jpg');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `ftiaxno_lekseis`
-- 

DROP TABLE IF EXISTS `ftiaxno_lekseis`;
CREATE TABLE IF NOT EXISTS `ftiaxno_lekseis` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `big_word` varchar(100) NOT NULL default '',
  `small_words` text NOT NULL,
  `correct_words` varchar(100) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `ftiaxno_lekseis`
-- 

INSERT INTO `ftiaxno_lekseis` VALUES (1, 1, 'καμηλοπάρδαλη', 'μήλο;πόδι;πάνω\r\nκαμήλα;κάτι;ήλιος\r\nκαλή;δάδα;λάδι\r\nαπαλή;κάτω;μέλι', 'μήλο;καμήλα;καλή;απαλή', '1_kamilopardali.jpg', 1);
INSERT INTO `ftiaxno_lekseis` VALUES (2, 1, 'στρουθοκάμηλος', 'καλός;πόρτα;ρολόι\r\nρόλος;κήπος;λαγός\r\nθυμός;πέτρα;κουτί\r\nλύκος;καφές;ήλιος', 'καλός;ρόλος;θυμός;λύκος', '1_strouthokamilos.jpg', 0);
INSERT INTO `ftiaxno_lekseis` VALUES (3, 1, 'ιπποπόταμος', 'ποτάμι;ράφι;λεμόνι\r\nρόδα;ίππος;πάλι', 'ποτάμι;ίππος', '1_ippo.jpg', 1);
INSERT INTO `ftiaxno_lekseis` VALUES (4, 3, 'στρουθοκάμηλος', 'καλός;πόρτα;ρολόι\r\nρόλος;κήπος;λαγός\r\nθυμός;πέτρα;κουτί\r\nλύκος;καφές;ήλιος', 'καλός;ρόλος;θυμός;λύκος', '3_stroutho.jpg', 1);
INSERT INTO `ftiaxno_lekseis` VALUES (5, 10, 'πολυκατοικία', 'πολύ;πόλος;πολλά\r\nκατοικία;κατοικώ;κάτοικος\r\nκαι;καλά;κάτω', 'πολύ;κατοικία;και', '', 1);
INSERT INTO `ftiaxno_lekseis` VALUES (6, 10, 'σκουλικομερμιγκότρυπα', 'σκουλίκι;\r\nμερμιγκι;\r\nτρύπα', 'τρύπα;σκουλίκι', '10_thm-sk18.jpg', 1);
INSERT INTO `ftiaxno_lekseis` VALUES (7, 18, 'παπαγαλοκωστας', 'κωστας;\r\nνικος;\r\nπαπα;\r\n\r\nπαπα;\r\nβασω;\r\nκωστης;\r\n\r\nκώστας;\r\nπαπα;\r\nγαλοπουλα;', 'παπα;γαλο;κωστας', '18_top_branding_jpg.jpg', 1);

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `maimou`
-- 

DROP TABLE IF EXISTS `maimou`;
CREATE TABLE IF NOT EXISTS `maimou` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `leksi` varchar(20) NOT NULL default '',
  `ermineia` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `maimou`
-- 

INSERT INTO `maimou` VALUES (1, 1, 'ΔΑΣΟΣ', 'Πολλά δέντρα μαζί');
INSERT INTO `maimou` VALUES (4, 1, 'ΔΕΝΤΡΟ', 'Απαραίτητο στην φύση');
INSERT INTO `maimou` VALUES (3, 1, 'ΘΑΜΝΟΣ', 'Είναι πράσινος');
INSERT INTO `maimou` VALUES (5, 1, 'ΚΑΝΑΤΑ', 'Βάζουμε νερό από αυτήν');
INSERT INTO `maimou` VALUES (6, 1, 'ΑΛΕΠΟΥ', 'Έξυπνο θηλαστικό');
INSERT INTO `maimou` VALUES (7, 1, 'ΓΑΝΤΙ', 'Μας προστατεύει απο το κρύο');
INSERT INTO `maimou` VALUES (8, 1, 'ΚΑΛΤΣΑ', 'Μας προστατεύει απο το κρύο');
INSERT INTO `maimou` VALUES (9, 1, 'ΦΑΓΗΤΟ', 'Απαραίτητο');
INSERT INTO `maimou` VALUES (10, 1, 'ΠΟΥΛΙΑ', 'Μπορεί να είναι και αποδημητικά');
INSERT INTO `maimou` VALUES (11, 1, 'ΚΛΑΔΙ', 'Πάνω στο δέντρο');
INSERT INTO `maimou` VALUES (12, 1, 'ΜΠΑΜΠΑΣ', 'Είναι παντρεμένος με τη Μαμά');
INSERT INTO `maimou` VALUES (13, 1, 'ΜΠΟΥΤΙ', 'Ωραίο του κοτόπουλου');
INSERT INTO `maimou` VALUES (14, 1, 'ΠΙΘΗΚΟΣ', 'Χαζό ζώο');
INSERT INTO `maimou` VALUES (15, 1, 'ΗΧΕΙΑ', 'Τα χρησιμοποιούμε για να ακούμε');
INSERT INTO `maimou` VALUES (16, 1, 'ΑΧΛΑΔΙΑ', 'Νόστιμο φρούτο');
INSERT INTO `maimou` VALUES (17, 1, 'ΝΤΟΜΑΤΑ', 'Κόκκινο λαχανικό');
INSERT INTO `maimou` VALUES (18, 1, 'ΕΛΑΦΙ', 'Το κυνηγούν πολύ');
INSERT INTO `maimou` VALUES (19, 1, 'ΑΚΡΙΔΑ', 'Έντομο που τρώει τις καλλιέργειες');
INSERT INTO `maimou` VALUES (20, 1, 'ΑΛΟΓΟ', 'Συμβολίζει την δύναμη');
INSERT INTO `maimou` VALUES (21, 1, 'ΨΑΡΙ', 'Περιέχει φώσφορο');
INSERT INTO `maimou` VALUES (22, 1, 'ΓΑΤΑ', 'Είναι πονηρή');
INSERT INTO `maimou` VALUES (23, 1, 'ΣΚΥΛΟΣ', 'Καλύτερος φίλος του ανθρώπου');
INSERT INTO `maimou` VALUES (24, 1, 'ΚΟΤΑ', 'Κάνει αυγά');
INSERT INTO `maimou` VALUES (25, 1, 'ΠΡΟΒΑΤΟ', 'Βγάζει μαλλί');
INSERT INTO `maimou` VALUES (26, 1, 'ΠΟΝΤΙΚΙ', 'Τρώει τυρί');
INSERT INTO `maimou` VALUES (27, 1, 'ΧΕΛΩΝΑ', 'Καρέτα - Καρέτα');
INSERT INTO `maimou` VALUES (28, 1, 'ΚΕΡΙ', 'Φωτίζει όμορφα');
INSERT INTO `maimou` VALUES (29, 1, 'ΨΩΜΙ', 'Το τρώμε κάθε πρωί');
INSERT INTO `maimou` VALUES (30, 1, 'ΝΕΡΟ', 'Το χρειαζόμαστε');
INSERT INTO `maimou` VALUES (31, 1, 'ΦΡΟΥΤΑ', 'Νόστιμα');
INSERT INTO `maimou` VALUES (32, 1, 'ΦΑΣΟΛΙ', 'Όσπριο');
INSERT INTO `maimou` VALUES (33, 1, 'ΚΑΡΤΕΛΑ', 'Ομαδοποεί');
INSERT INTO `maimou` VALUES (34, 3, 'ΔΕΝΤΡΟ', 'Φυτό του οποίου ο κορμός αρχίζει να διακλαδίζεται σε κάποιο ύψος.');
INSERT INTO `maimou` VALUES (35, 3, 'ΠΑΠΠΟΥΣ', 'Ο πατέρας του πατέρα ή της μητέρας μας.');
INSERT INTO `maimou` VALUES (36, 3, 'ΚΟΤΕΤΣΙ', 'χώρος όπου φυλάγουν τις κότες');
INSERT INTO `maimou` VALUES (37, 5, 'ΚΟΚΟΡΑΣ', 'Ο ΑΡΧΗΓΟΣ ΤΟΥ ΚΟΤΕΤΣΙΟΥ');
INSERT INTO `maimou` VALUES (38, 5, 'ΔΑΣΟΣ', 'ΜΕΡΟΣ ΜΕ ΠΟΛΛΑ ΔΕΝΔΡΑ');
INSERT INTO `maimou` VALUES (39, 5, 'ΓΑΤΑΚΙ', 'ΤΟ ΜΙΚΡΟ ΤΗΣ ΓΑΤΑΣ');
INSERT INTO `maimou` VALUES (40, 5, 'ΚΩΣΤΑΚΗ', 'Ο ΜΙΚΡΟΣ ΚΩΣΤΑΣ');
INSERT INTO `maimou` VALUES (41, 6, 'ΠΟΥΛΙ', 'Πετά στον ουρανό');
INSERT INTO `maimou` VALUES (42, 6, 'ΨΑΡΙ', 'Κολυμπά στη θάλασσα');
INSERT INTO `maimou` VALUES (43, 10, 'ΠΟΤΗΡΙ', 'σκεύος που το χρησιμοποιούμε να πίνουμε υγρά');
INSERT INTO `maimou` VALUES (44, 10, 'KOYΠΑ', 'σκεύος που πίνουμε συνήθως καφέ');
INSERT INTO `maimou` VALUES (45, 10, 'ΠΙΑΤΟ', 'μέσα σ''αυτό τρώμε το φαγητό μας');
INSERT INTO `maimou` VALUES (46, 10, 'ΜΟΛΥΒΙ', 'το χρησιμοποιούμε για να γράφουμε');
INSERT INTO `maimou` VALUES (47, 10, 'ΓΥΑΛΙΑ', 'μ΄ αυτά βλέπουμε καλύτερα');
INSERT INTO `maimou` VALUES (48, 10, 'ΤΡΑΠΕΖΙ', 'σ΄αυτό τρώμε, διαβάζουμε, παίζουμε επιτραπέζια παιχνίδια');
INSERT INTO `maimou` VALUES (49, 10, 'ΚΙΝΗΤΟ', 'το χρησιμοποιούμε γαι να επικοινωνούμε όταν είμαστε έξω από το σπίτι');
INSERT INTO `maimou` VALUES (50, 18, 'δεντρο', 'Φυτό του οποίου ο κορμός αρχίζει να διακλαδίζεται');
INSERT INTO `maimou` VALUES (51, 18, 'κοτέτσι', 'χώρος που φυλλάσονται οι κότες');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `mpalonia`
-- 

DROP TABLE IF EXISTS `mpalonia`;
CREATE TABLE IF NOT EXISTS `mpalonia` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `lekseis_correct` text NOT NULL,
  `lekseis_false` text NOT NULL,
  `letter` varchar(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=0;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `mpalonia`
-- 

INSERT INTO `mpalonia` VALUES (1, 1, 'ανεμώνη;αλάτι;άνεμος;παπαρούνα', 'ηχείο;ποντίκι;τηλέφωνο;δίσκος', 'Α');
INSERT INTO `mpalonia` VALUES (2, 1, 'διαβήτης;αχλάδι;Δάφνη;εφημερίδα;δέντρο;τετράδιο;πυξίδα', 'μολύβι;καλάθι;φόρεμα;μήλο;πρόβατο', 'Δ');
INSERT INTO `mpalonia` VALUES (5, 1, 'φακός;Φάνης;ελέφαντας;σύννεφο;σφυρίχτρα;σφουγγάρι;φωλιά', 'άλογο;καλάθι;ρολόι;μήλο;πηγάδι', 'Φ');
INSERT INTO `mpalonia` VALUES (6, 1, 'ήλιος;ποτήρι;Ελένη;δύναμη;ηφαίστειο;ήμερος', 'κάτι;ποτάμι;καρέκλα;λιβάδι', 'Η');
INSERT INTO `mpalonia` VALUES (7, 3, 'φασόλι;καφές;φούρνος;φράουλα;ελάφι;αφρός', 'καρότο;πεπόνι;χέρι;άλογο', 'Φ');
INSERT INTO `mpalonia` VALUES (8, 3, 'μαμά;ποτάμι;Μάρκος;μολύβι;καμήλα', 'σκίουρος;κότα;άλογο;βοσκός', 'Μ');
INSERT INTO `mpalonia` VALUES (9, 5, 'μέλι;Λέλα;μητέρα;;Αλέξης;;περιστέρι;Ελένη;;κερί;', 'κότα;γάλα;καράβι;τυρί;', 'ε');
INSERT INTO `mpalonia` VALUES (10, 11, 'νερό;Νέλη;αέρας;ξερός;λελέκι;πένα;Ρένα', 'κότα;ποτάμι;θάλασσα;κορίτσι;λιμάνι', 'ε');
INSERT INTO `mpalonia` VALUES (11, 6, 'παράθυρο;πουλί;πειρατής;πόνος;πιλότος', 'βιβλίο;τετράδιο;χέρι', 'Π');
INSERT INTO `mpalonia` VALUES (12, 10, 'ψωμί;ταψί;καψιμί;ανηψιά', 'καφές;αλοιφή;τραπέζι;καλός', 'Ψ');
INSERT INTO `mpalonia` VALUES (13, 10, 'ουρά;καρύδι;παραμύθι;κρύβω', 'ποτήρι;πόρτα;μαρίδα;', 'Y');
INSERT INTO `mpalonia` VALUES (14, 18, 'φασόλι;καφές;φωφώ;φίκος;ελάφι', 'καρότο;πεπόνι;χέρι;άλογο', 'Φ');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `protaseis_mperdeuthkan`
-- 

DROP TABLE IF EXISTS `protaseis_mperdeuthkan`;
CREATE TABLE IF NOT EXISTS `protaseis_mperdeuthkan` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `lekseis` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  `sound` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `protaseis_mperdeuthkan`
-- 

INSERT INTO `protaseis_mperdeuthkan` VALUES (1, 1, 'Οι;μπότες;είναι;ωραίες', '1_mpotes.jpg', '1_mpotes.mp3', 1);
INSERT INTO `protaseis_mperdeuthkan` VALUES (2, 1, 'Οι;ντομάτες;είναι;κόκκινες', '1_ntomates.jpg', '1_ntomates.mp3', 1);
INSERT INTO `protaseis_mperdeuthkan` VALUES (4, 3, 'Η;γραμμούλα;μας;έφτασε;στο;χωριό.', '3_village.jpg', '3_grammi.mp3', 1);
INSERT INTO `protaseis_mperdeuthkan` VALUES (8, 10, 'η;έρημος;της;Σαχάρας;εχει;χρυσή;άμμο', '10_5peti.jpg', '', 1);
INSERT INTO `protaseis_mperdeuthkan` VALUES (9, 10, 'το;Μαρόκο;είναι;μια;χώρα;της;βόρειας;Αφρικής', '10_Carte.jpg', '', 1);
INSERT INTO `protaseis_mperdeuthkan` VALUES (10, 18, 'Η;γραμμουλα;μας;έφτασε;στο;χωριό', '18_newpi_1007_01.gif', '18_aithousa.mp3', 1);

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `syllabes_mperdeuthkan`
-- 

DROP TABLE IF EXISTS `syllabes_mperdeuthkan`;
CREATE TABLE IF NOT EXISTS `syllabes_mperdeuthkan` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `syllables` varchar(255) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `sound` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `syllabes_mperdeuthkan`
-- 

INSERT INTO `syllabes_mperdeuthkan` VALUES (1, 1, 'μπό;τες', '1_mpotes.jpg', '1_mpotes.mp3', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (2, 1, 'ντο;μά;τες', '1_ntomates.jpg', '1_ntomates.mp3', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (3, 1, 'παι;διά', '1_kids.jpg', '1_kids.mp3', 0);
INSERT INTO `syllabes_mperdeuthkan` VALUES (11, 6, 'δο;κι;μή', '6_τεστ2.JPG', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (5, 3, 'Αί;σω;πος', '3_aisopos.jpg', '3_aisopos.mp3', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (6, 3, 'γά;ι;δα;ρος', '3_gaidaros.jpg', '3_gaidaros.mp3', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (7, 4, 'Πο;δή;λα;το', '4_Μπλε λόφοι.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (12, 6, 'δο;κι;μά;ζω', '6_τεστ1.JPG', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (13, 6, 'πε;ρή;φα;νος', '6_τεστ3.JPG', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (21, 15, 'ΠΕ;ΡΙ;ΣΤΕ;ΡΙ', '15_peristeri.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (15, 10, 'στά;δι;ο', '10_IMAGE0020.JPG', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (22, 15, 'ο;μπρε;λα', '15_umbrela.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (17, 12, 'μα;ρε;φό', '12_1dress36-thumb.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (18, 10, 'Λου;λού;δι', '10_1134394718.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (23, 17, 'ΠΟ;ΣΕΙ;ΔΩ;ΝΑΣ', '17_Blue hills.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (24, 17, 'ΚΑ;ΤΑ;ΔΥ;ΣΗ', '17_0701whale1.jpg', '', 1);
INSERT INTO `syllabes_mperdeuthkan` VALUES (27, 18, 'πα;πα;λος;γα;', '', '', 1);

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `taksinomisi`
-- 

DROP TABLE IF EXISTS `taksinomisi`;
CREATE TABLE IF NOT EXISTS `taksinomisi` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `tambela` varchar(100) NOT NULL default '',
  `lekseis` text NOT NULL,
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `taksinomisi`
-- 

INSERT INTO `taksinomisi` VALUES (1, 1, 'α', 'ανεμώνη;αμυγδαλιά;αχλαδιά', 1);
INSERT INTO `taksinomisi` VALUES (2, 1, 'β', 'βυσσινιά;βασιλικός;βλήτο', 1);
INSERT INTO `taksinomisi` VALUES (4, 3, 'Α', 'ανεμώνη;Αλεξάνδρα;αεροπλάνο', 1);
INSERT INTO `taksinomisi` VALUES (5, 3, 'Β', 'Βασίλης;βάζο;βάτραχος;βιβλίο', 1);
INSERT INTO `taksinomisi` VALUES (6, 5, 'ΔΗΜΟΤΙΚΟ ΣΧΟΛΕΙΟ', 'ΔΗΜΟΤΙΚΟ;ΣΧΟΛΕΙΟ', 1);
INSERT INTO `taksinomisi` VALUES (7, 5, 'ΠΕΙΡΑΜΑΤΙΚΟ ΣΧΟΛΕΙΟ ΖΑΚΥΝΘΟΥ', 'ΠΕΙΡΑΜΑΤΙΚΟ;ΣΧΟΛΕΙΟ;ΖΑΚΥΝΘΟΥ', 1);
INSERT INTO `taksinomisi` VALUES (8, 5, 'ΒΑΝΑΤΟ ΖΑΚΥΝΘΟΥ', 'ΒΑΝΑΤΟ;ΖΑΚΥΝΘΟΥ', 1);
INSERT INTO `taksinomisi` VALUES (9, 5, 'ΑΥΡΙΑΚΟΣ ΒΑΝΑΤΟ ΖΑΚΥΝΘΟΣ', 'ΑΥΡΙΑΚΟΣ;ΒΑΝΑΤΟ;ΖΑΚΥΝΘΟΣ', 1);
INSERT INTO `taksinomisi` VALUES (10, 10, 'K', 'κίτρο;κουνουπίδι;καναρίνι', 1);
INSERT INTO `taksinomisi` VALUES (11, 10, 'N', 'νούφαρο;ανανάς;μπανάνα', 1);
INSERT INTO `taksinomisi` VALUES (12, 13, 'A', 'ανεμώνη;αμυγδαλιά;αγριελιά', 1);
INSERT INTO `taksinomisi` VALUES (13, 13, 'B', 'βερυκοκιά;βρύα;βαμβάκι', 1);
INSERT INTO `taksinomisi` VALUES (14, 13, 'Γ', 'γαρυφαλιά;γογγύλι', 1);
INSERT INTO `taksinomisi` VALUES (15, 18, 'Α', 'ανεμώνη;Αλεξάνδρα;αεροπλάνο', 1);
INSERT INTO `taksinomisi` VALUES (16, 18, 'Β', 'Βασίλης;βάζο;βάτραχος;βιβλίο', 1);

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `teachers`
-- 

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(6) NOT NULL auto_increment,
  `lastname` varchar(100) NOT NULL default '',
  `firstname` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `school` varchar(100) NOT NULL default '',
  `password` varchar(11) NOT NULL default '',
  `reg_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 PACK_KEYS=1;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `teachers`
-- 

INSERT INTO `teachers` VALUES (1, 'Φώτης', 'Αθανάσιος', 'tfotis@tessera.gr', '1ο Λύκειο Τούμπας', '123', '2006-01-01');
INSERT INTO `teachers` VALUES (2, 'Εφόπουλος', 'Βασίλης', 'efop@tessera.gr', '1ο Λύκειο Θεσσαλονίκης', '123', '2006-01-09');
INSERT INTO `teachers` VALUES (3, 'Γκαιντέ', 'Σοφία', 'gaide@tessera.gr', 'tessera', '123', '2006-01-31');
INSERT INTO `teachers` VALUES (4, 'Εφόπουλος', 'Βασίλειος', 'efop@tessera.gr', '6o TEE Εσπερινό', 'efop123!', '2006-02-13');
INSERT INTO `teachers` VALUES (5, 'Αρβανιτάκης', 'Νικόλαος', 'nikarvanit@sch.gr', 'Σχολικός Σύμβουλος', 'να1591', '2006-02-21');
INSERT INTO `teachers` VALUES (6, 'ΑΡΓΥΡΗΣ', 'ΜΙΧΑΛΗΣ', 'margyris@sch.gr', '20o Δημ. Σχ. Αγίου Δημητρίου', '123μα', '2006-02-22');
INSERT INTO `teachers` VALUES (7, 'a', 'a', 'abalas@mycosmos.gr', 'a', '123', '2006-02-24');
INSERT INTO `teachers` VALUES (8, 'a', 'a', 'abalas@mycosmos.gr', 'a', 'tt', '2006-02-24');
INSERT INTO `teachers` VALUES (9, 'asda', 'dasda', 'dasdad', 'asda', 'ss', '2006-02-24');
INSERT INTO `teachers` VALUES (10, 'Kanelli', 'Aggeliki', 'vana@otenet.gr', 'PI', '1958', '2006-02-24');
INSERT INTO `teachers` VALUES (11, 'sxolikos symvoulos', 'zakynthos', 'nikarvanit@sch.gr', '1hperifereiaazak', '29100', '2006-02-27');
INSERT INTO `teachers` VALUES (12, 'Ματή', 'Ελένη', 'grss@dipe.mag.sch.gr', '1ο Βόλου', '38333', '2006-02-27');
INSERT INTO `teachers` VALUES (13, 'Καψασκη', 'Βανα', 'vana@otenet.gr', '10', '1988', '2006-03-27');
INSERT INTO `teachers` VALUES (14, 'Κελεσίδης', 'Ευάγγελος', 'ekelesidis@sch.gr', '3ο Δημ. Θεσσαλονίκης', '3', '2006-04-03');
INSERT INTO `teachers` VALUES (15, 'ΚΟΥΝΕΛΑΚΗΣ', 'ΚΩΣΤΑΣ', 'mail@dim-asvest.thess.sch.gr', 'Δ.Σχ. Ασβεστοχωρίου', '3', '2006-04-06');
INSERT INTO `teachers` VALUES (16, 'trifonidis', 'trifon', '1@1.com', 'a nikaia', '1234', '2006-04-30');
INSERT INTO `teachers` VALUES (17, 'ΤΕΟ', 'ΔΕΣ', 'WWW.ΑΡΒΑΝ@.GR', 'E', '123', '2006-05-19');
INSERT INTO `teachers` VALUES (18, 'Ιατρού', 'Κων/νος', 'kiat@pi-schools.gr', 'Παιδαγωγικό Ινστιτούτο', '11111', '2006-06-19');
