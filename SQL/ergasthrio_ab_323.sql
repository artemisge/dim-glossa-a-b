-- 
-- Βάση: `ergasthrio_ab`
-- 

-- 
-- Δομή Πίνακα για τον Πίνακα `activities`
-- 

DROP TABLE IF EXISTS `activities`;
CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(3) NOT NULL default '0',
  `name` varchar(255) NOT NULL default '',
  `activity_table` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

-- 
-- 'Αδειασμα δεδομένων του πίνακα `activities`
-- 

INSERT INTO `activities` VALUES (1, 'Οι συλλαβές μπερδεύτηκαν', 'syllabes_mperdeuthkan');
INSERT INTO `activities` VALUES (2, 'Φτιάχνω τις λέξεις', 'ftiaxno_lekseis');
INSERT INTO `activities` VALUES (3, 'Ταξινόμηση', 'taksinomisi');
INSERT INTO `activities` VALUES (4, 'Οι προτάσεις μπερδεύτηκαν', 'protaseis_mperdeuthkan');
INSERT INTO `activities` VALUES (5, 'Τα μπαλόνια', 'mpalonia');
INSERT INTO `activities` VALUES (6, 'Βρίσκω τις συλλαβές', 'brisko_syllabes');
INSERT INTO `activities` VALUES (7, 'Ακροστοιχίδα', 'akrostoixida');
INSERT INTO `activities` VALUES (8, 'Εικόνες', 'eikones');
INSERT INTO `activities` VALUES (9, 'Μαϊμού', 'maimou');

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `akrostoixida`
-- 

DROP TABLE IF EXISTS `akrostoixida`;
CREATE TABLE IF NOT EXISTS `akrostoixida` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `data_text` text NOT NULL,
  `data_images` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `brisko_syllabes`
-- 

DROP TABLE IF EXISTS `brisko_syllabes`;
CREATE TABLE IF NOT EXISTS `brisko_syllabes` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `word_without` varchar(255) NOT NULL default '',
  `syllables` varchar(255) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `eikones`
-- 

DROP TABLE IF EXISTS `eikones`;
CREATE TABLE IF NOT EXISTS `eikones` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `story` int(3) NOT NULL default '0',
  `word` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `ftiaxno_lekseis`
-- 

DROP TABLE IF EXISTS `ftiaxno_lekseis`;
CREATE TABLE IF NOT EXISTS `ftiaxno_lekseis` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `big_word` varchar(100) NOT NULL default '',
  `small_words` text NOT NULL,
  `correct_words` varchar(100) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `maimou`
-- 

DROP TABLE IF EXISTS `maimou`;
CREATE TABLE IF NOT EXISTS `maimou` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `leksi` varchar(20) NOT NULL default '',
  `ermineia` varchar(255) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `mpalonia`
-- 

DROP TABLE IF EXISTS `mpalonia`;
CREATE TABLE IF NOT EXISTS `mpalonia` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `lekseis_correct` text NOT NULL,
  `lekseis_false` text NOT NULL,
  `letter` varchar(2) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `protaseis_mperdeuthkan`
-- 

DROP TABLE IF EXISTS `protaseis_mperdeuthkan`;
CREATE TABLE IF NOT EXISTS `protaseis_mperdeuthkan` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `lekseis` text NOT NULL,
  `image` varchar(100) NOT NULL default '',
  `sound` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `syllabes_mperdeuthkan`
-- 

DROP TABLE IF EXISTS `syllabes_mperdeuthkan`;
CREATE TABLE IF NOT EXISTS `syllabes_mperdeuthkan` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `syllables` varchar(255) NOT NULL default '',
  `image` varchar(100) NOT NULL default '',
  `sound` varchar(100) NOT NULL default '',
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `taksinomisi`
-- 

DROP TABLE IF EXISTS `taksinomisi`;
CREATE TABLE IF NOT EXISTS `taksinomisi` (
  `id` int(6) NOT NULL auto_increment,
  `teacher_id` int(6) NOT NULL default '0',
  `tambela` varchar(100) NOT NULL default '',
  `lekseis` text NOT NULL,
  `show_record` int(1) NOT NULL default '1',
  PRIMARY KEY  (`id`),
  KEY `teacher_id` (`teacher_id`)
) TYPE=MyISAM;

-- --------------------------------------------------------

-- 
-- Δομή Πίνακα για τον Πίνακα `teachers`
-- 

DROP TABLE IF EXISTS `teachers`;
CREATE TABLE IF NOT EXISTS `teachers` (
  `id` int(6) NOT NULL auto_increment,
  `lastname` varchar(100) NOT NULL default '',
  `firstname` varchar(100) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `school` varchar(100) NOT NULL default '',
  `password` varchar(11) NOT NULL default '',
  `reg_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;
