<?php 

	session_start();

	session_destroy();
	
	$_SESSION['teacher_id'] = '';
	$_SESSION['isUser'] = 0;
	
	header('Content-Type: text/html; charset=utf-8');

	include "language/ell.php";
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>ΤΟ ΕΡΓΑΣΤΗΡΙΟ ΤΟΥ ΔΑΣΚΑΛΟΥ - Εισαγωγή στο Σύστημα</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="scripts/scripts.js"></script>
</head>

<body style="margin:0px;" onload="javascript:document.forms[0].elements[0].focus();">

<br>

<div align="center">


<form name="login_form" method="post" action="login_check.php" onSubmit="return(checkLogin());">


<table width="200" border="0" cellpadding="4" cellspacing="2">
<tr>
	<td width="50%" align="right" class="td1"><?php echo _ID; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="form_id" name="form_id" type="text" maxlength="11" style="width:80px;"></td>
</tr>
<tr>
	<td width="50%" align="right" class="td1"><?php echo _PASSWORD; ?>:&nbsp;</td>
	<td width="50%" align="left"  class="td2"><input id="form_pass" name="form_pass" type="password" maxlength="11" style="width:80px;"></td>
</tr>
<tr>
	<td width="100%" align="center" colspan="2">
	<input type="submit" name="submit" value="<?php echo _LOGIN; ?>">
	</td>
</tr>
</table>

</form>

<br>


<a href="register.php"><?php echo _CREATEACCOUNT; ?></a>


<br>


<?php

	if (isset($_GET['error']))
	{
		echo '<br>';

		if ($_GET['error'] == 1) echo '<p><font class="red1">'._NOSUCHID.'</font>';
		if ($_GET['error'] == 2) echo '<p><font class="red1">'._WRONGPASSWORD.'</font>';
	}

?>


</div>

</body>

</html>