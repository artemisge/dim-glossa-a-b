var subWindow = null;
var subWindow2 = null;

document.onkeydown = checkKey;

function checkKey(e)
{
	if (!e) var e = window.event;

	var oTarget = (e.target) ? e.target : e.srcElement;
	
	if (e.shiftKey)
	{
		// SHIFT + ~
		if (e.keyCode == 192)
		{
			if (!oTarget.type)
			{
				alert('ΕΡΓΑΣΤΗΡΙΟ ΤΟΥ ΔΑΣΚΑΛΟΥ\nTessera Multimedia S.A.');
			}
		}
	}
}

function disableItem(elem)
{
	if (document.forms[0].elements[elem])
	{
		document.forms[0].elements[elem].style.backgroundColor = "#DED9D0";
		document.forms[0].elements[elem].disabled = true;
	}
}

function enableItem(elem)
{
	if (document.forms[0].elements[elem])
	{
		document.forms[0].elements[elem].style.backgroundColor = "#ffffff";
		document.forms[0].elements[elem].disabled = false;
	}
}


function openWindow(file,width,height,top,left)
{
	var x = window.screen.width;
	var y = window.screen.height;

	var posx = (x-width)/2;
	var posy = (y-height)/2;
	
	subWindow2 = window.open(file,'','width='+width+',height='+height+',top='+posy+',left='+posx+',location=no,menubar=no,resizable=yes,scrollbars=no,status=yes,toolbar=no,fullscreen=no');
}

function popModal(file,width,height)
{	
	var x = window.screen.width;
	var y = window.screen.height;

	var posx = (x-width)/2;
	var posy = (y-height)/2;

	subWindow = window.open(file,'child','width='+width+',height='+height+',top='+posy+',left='+posx+',location=no,menubar=no,resizable=yes,scrollbars=yes,status=no,toolbar=no,fullscreen=no');
	subWindow.focus();
}

function checkLogin()
{
	if (document.getElementById('form_id').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το ID");
		return false;
	}
	else if (document.getElementById('form_pass').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το Password");
		return false;
	}
	else
	{
		return true;
	}
}

function checkRegistration()
{
	if (document.getElementById('lastname').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το επώνυμό σας");
		return false;
	}
	else if (document.getElementById('firstname').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το όνομά σας");
		return false;
	}
	else if (document.getElementById('email').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το email σας");
		return false;
	}
	else if (document.getElementById('school').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε το σχολείο σας");
		return false;
	}
	else if (document.getElementById('password1').value.length == 0)
	{
		alert("Παρακαλώ συμπληρώστε τον κωδικό");
		return false;
	}
	else if (document.getElementById('password2').value.length == 0)
	{
		alert("Παρακαλώ επαληθεύστε τον κωδικό");
		return false;
	}
	else if (document.getElementById('password1').value != document.getElementById('password2').value)
	{
		alert("Τα πεδία των κωδικών δεν ταιριάζουν μεταξύ τους");
		return false;
	}
	else
	{
		return true;
	}
}

function redirectTo(page)
{
	window.location = page;
}

function changeOrder(name)
{
	document.getElementById('order').value = name;
	document.forms[document.getElementById('order').form.id].submit();
}

function confirmDelete(id)
{
	var fRet; 
	
	fRet = confirm('Διαγραφή της εγγραφής ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('delete').value = id;
		document.forms[document.getElementById('delete').form.id].submit();
	}
}

function changeColor(line,color)
{
	//document.getElementById(line).style.backgroundColor = color;
	return;
}

function changeImageDimensions(img_id)
{
	var w = document.getElementById(img_id).width;

	//alert(w);

	if (w != 0)
	{
		var percent = w/100;

		if (percent > 1)
		{
			document.getElementById(img_id).width = document.getElementById(img_id).width / percent;
		}

		document.getElementById(img_id).style.visibility = "visible";
	}
	else
	{
		document.getElementById(img_id).style.visibility = "hidden";
		window.setTimeout("changeImageDimensions('"+img_id+"')", 100);
	}
}

function deleteImage()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή της Εικόνας ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('dummy').src = '../images/blank.gif';
		document.getElementById('image').value = '';
	}
}

function deleteSound()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή του Ήχου ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('empty').value = '';
		document.getElementById('sound').value = '';
	}
}

function deleteMultipleImages()
{
	var fRet; 
	
	fRet = confirm('Διαγραφή Όλων των Εικόνων ?'); 
	
	if (fRet == false)
	{
		return;
	}
	else
	{
		document.getElementById('tmp').value = '';
		document.getElementById('data_images').value = '';
	}
}