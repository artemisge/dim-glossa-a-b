<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Εισαγωγή Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL INSERT RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'INSERT INTO akrostoixida (teacher_id, data_text, data_images, image) VALUES ('.$_SESSION['teacher_id'].', "'.trim($_POST['data_text']).'", "'.trim($_POST['data_images']).'", "'.$_POST['image'].'")';

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


echo '	<form id="mc" name="mc" method="post" action="insert_akrostoixida.php">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._INSERTAKROSTOIXIDA.'</td>
		</tr>
		</table>';

echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._WORDS.':</td>
			<td class="td2" width="255"><textarea id="data_text" name="data_text" style="width:250px;" rows="7"></textarea></td>
			<td class="td8">'._WORDSDESC3.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGES.':</td>
			<td class="td2" width="255">
			<textarea id="tmp" name="tmp" style="width:250px;" rows="7" disabled></textarea>
			<input type="hidden" id="data_images" name="data_images" value="">
			</td>
			<td class="td8"><a href="javascript:;" onclick="openWindow(\'uploadMultiplePhotos.php?fld=akrostoixida\',250,310);">'._INSERTIMAGES.'</a></td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI('','').'</td>
			<td class="td8"><a href="javascript:;" onclick="'.uploadPhotoLink('akrostoixida').'">'._INSERTIMAGE.'</a></td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>