<?php

session_start();
	
header('Content-Type: text/html; charset=utf-8');

include "../API.php";

include "../language/ell.php";

if (!$_SESSION['isUser']) unauthorized();


$error_number = $_FILES['file1']['error'];
$server_path = $_SERVER['SCRIPT_FILENAME'];
$photos_path = substr($server_path,0,strpos($server_path,'teacher')).'media/images/'.$_GET['fld'].'/';


if ($_POST['action']) 
{ 	
	// Όνομα αρχείου
	$upload_name = $_SESSION['teacher_id'].'_'.$_FILES['file1']['name'];

	// Προσωρινό όνομα αρχείου
	$source = $_FILES['file1']['tmp_name']; 

	// Προορισμός αρχείου
	$dest = $photos_path.$upload_name;

	// Άλλαξε τα δικαιώματα σε 777 (απο 755) για να μπορέσουμε να αποθηκεύσουμε το αρχείο
	$chmod_ok = chmod($photos_path, 0777);

	// Αν το αρχείο μεταφέρθηκε στον φάκελο
	// άλλαξε την εικόνα και το πεδίο με το όνομά του στο _parent παράθυρο
	// και exit
	// αλλιώς εμφάνισε μύνημα βάσει του error number
	$move_ok = move_uploaded_file($source,$dest);

	if ($move_ok) 
	{ 
		// Άλλαξε τα δικαιώματα ξανά σε 755
		chmod($dest, 0755);
		chmod($photos_path, 0755);

		
		?>
		<SCRIPT>
		
		window.opener.document.getElementById('image').value = "<?php echo $upload_name; ?>";
		window.opener.document.getElementById('dummy').src = "../../media/images/<?php echo $_GET['fld']; ?>/<?php echo $upload_name; ?>";
		window.close();
		
		</SCRIPT>
		<?php

	} 
	else 
	{ 
		// Άλλαξε τα δικαιώματα ξανά σε 755
		chmod($photos_path, 0755);

		if ($error_number == 2)
		{
			$error =  '<span class="red1">'._MAXFILEBYTES.' '.$_POST['MAX_FILE_SIZE'].' bytes</span>';
		}
		else if ($error_number == 4)
		{
			$error =  '<span class="red1">'._CHOOSEFILE.'</span>';
		}
		else if ($error_number == 7) // Works Only With PHP > 5.1.0
		{
			$error =  '<span class="red1">'._STOREERROR.' '.$upload_name.'</span>';
		}
	}
} 

?>

<HTML> 
<HEAD> 
<TITLE>Αποστολή Φωτογραφίας</TITLE> 
</HEAD>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">

<BODY bgcolor="#FFFFFF">

<?php

	echo '<p>'._IMAGEFILENAMEDESC;

?>

<FORM METHOD="POST" ENCTYPE="multipart/form-data" ACTION="uploadPhoto.php?fld=<?php echo $_GET['fld']; ?>"> 

<INPUT TYPE="HIDDEN" NAME="action" VALUE="1"> 
<INPUT type="hidden" NAME="MAX_FILE_SIZE" value="100000">
<INPUT TYPE="FILE" NAME="file1" SIZE="30"><BR><BR> 
<INPUT TYPE="SUBMIT" VALUE="Αποστολή">

</FORM> 

<?php

	if (isset($error) && !empty($error))
	{
		echo $error;
	}
	else
	{
		echo '<span class="blue1">'._IMAGEFORMAT.'</span>';
	}

?>

</BODY> 

</HTML> 