<?php 

	session_start();
	
	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE eikones SET story = '.$_POST['story'].', word = "'.$_POST['word'].'", image = "'.$_POST['image'].'" WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT story, word, image FROM eikones WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_eikones.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITEIKONES.'</td>
		</tr>
		</table>';


if (empty($rs['image']))
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('eikones').'">'._INSERTIMAGE.'</a>';
}
else
{
	$image_link = '<a href="javascript:;" onclick="'.uploadPhotoLink('eikones').'">'._CHANGEIMAGE.'</a><br><a href="javascript:;" onclick="'.deleteImage().'">'._DELETEIMAGE.'</a>';
}


echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._STORY.':</td>
			<td class="td2" width="255"><input id="story" name="story" style="width:25px;" value="'.$rs['story'].'" maxlength="3"></td>
			<td class="td8">'._STORYDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._WORDSENTENCE.':</td>
			<td class="td2" width="255"><textarea id="word" name="word" style="width:250px;" rows="5">'.$rs['word'].'</textarea></td>
			<td class="td8">'._WORDSENTENCEDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._IMAGE.':</td>
			<td class="td2" width="255">'.getImageForEI($rs['image'],'eikones').'</td>
			<td class="td8">'.$image_link.'</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>