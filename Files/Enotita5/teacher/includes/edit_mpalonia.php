<?php 

	session_start();

	header('Content-Type: text/html; charset=utf-8');

	include "../API.php";

	include "../language/ell.php";

	if (!$_SESSION['isUser']) unauthorized(); 
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Επεξεργασία Στοιχείων</title>
<META http-equiv=Content-Type content="text/html; charset=UTF-8">
<META name="Author" content="Tessera Multimedia S.A.">
<link href="../styles/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="../scripts/scripts.js"></script>
</head>

<body style="margin:20px;">


<?php

// SQL UPDATE RECORDS /////////////////////////
if ($_POST)
{
	$sql = 'UPDATE mpalonia SET lekseis_correct = "'.$_POST['lekseis_correct'].'", lekseis_false = "'.$_POST['lekseis_false'].'" WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];

	$rs =& $dbconn->Execute($sql);

	if ($rs) echo '<SCRIPT language="Javascript">window.opener.document.forms[\'mu\'].submit();window.close();</SCRIPT>';
}


// SQL GET RECORDS /////////////////////////

$sql = 'SELECT lekseis_correct, lekseis_false, letter FROM mpalonia WHERE teacher_id = '.$_SESSION['teacher_id'].' AND id = '.$_GET['id'];
$rs =& $dbconn->GetRow($sql);
////////////////////////////////////////////


echo '	<form id="mc" name="mc" method="post" action="edit_mpalonia.php?id='.$_GET['id'].'">';

echo '	<table width="100%" border="0" cellpadding="4" cellspacing="2">
		<tr>
			<td width="100%" class="td6">'._EDITBALOONS.'</td>
		</tr>
		</table>';

echo '	<table width="100%" border="0" cellspacing="2" cellpadding="4">
		<tr>
			<td class="td7" width="110">'._CORRECTWORDS.':</td>
			<td class="td2" width="255"><input type="text" id="lekseis_correct" name="lekseis_correct" style="width:250px;" value="'.$rs['lekseis_correct'].'"></td>
			<td class="td8">'._CORRECTWORDSDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._FALSEWORDS.':</td>
			<td class="td2" width="255"><input type="text" id="lekseis_false" name="lekseis_false" style="width:250px;" value="'.$rs['lekseis_false'].'"></td>
			<td class="td8">'._FALSEWORDSDESC.'</td>
		</tr>
		<tr>
			<td class="td7" width="110">'._LETTER.':</td>
			<td class="td2" width="255"><input type="text" id="letter" name="letter" style="width:10px;" maxlength="1" value="'.$rs['letter'].'" disabled></td>
			<td class="td8">-</td>
		</tr>
		<tr> 
			<td align="center" valign="middle" colspan="3">
			<input type="submit" id="buttonDo" name="buttonDo" value="'._SAVE.'">
			</td>
		</tr>
		</table>';

echo '	</form>';


?>

</BODY>

</HTML>