<?php


include "adodb/adodb.inc.php";
//include "../adodb/adodb-pager.inc.php";

session_start();

$server = 'localhost';
$user = 'myname';
$pass = 'mypassword';
$database = 'ergasthrio_ab';


$dbconn = &ADONewConnection('mysql');
$dbconn->debug = false;	
$dbconn->Connect($server, $user, $pass, $database);

// WHICH VERSION OF MYSQL ?
// IF ABOVE 4.1, SET NAMES MUST BE USED
$server_info = mysql_get_server_info ();
$server_version = (int) $server_info{0};
$server_subversion = (int) $server_info{2};

if ($server_version >= 5)
{
	$dbconn->Execute("SET NAMES 'utf8'");
}
else if ($server_version == 4 && $server_subversion > 0)
{
	$dbconn->Execute("SET NAMES 'utf8'");
}
////////////////////////////////////////


function unauthorized()
{
    echo '<br>';
	echo '<div align="center">'._UNAUTHORIZEDACCESS.'</div>';
    exit;
}


function numberOfResults($start,$num_of_records,$total_records)
{
	if ($num_of_records == 0) return $out = '';

	$out .= _RECORDS.' '.($start+1).' '._OF.' '.($start+$num_of_records).' ('.$total_records.' '._TOTAL.')';

	return $out;
}


function navigator($form_name,$start,$num_of_records,$total_records,$limit)
{
	if ($num_of_records <= 0 || $num_of_records == $total_records) return;

	if ($start <= 0)
	{
		$dis1 = 'disabled';
	}
	else
	{
		$dis1 = '';
	}

	if ($start == $total_records - $num_of_records)
	{
		$dis2 = 'disabled';
	}
	else
	{
		$dis2 = '';
	}

	if ($start == $total_records - $num_of_records)
	{
		$prev_page = $total_records-$num_of_records-$limit;
	}
	else
	{
		$prev_page = $start-$num_of_records;
	}
	

	$next_page = $start+$num_of_records;

	$out = '<div align="center">
			<table width="220" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td width="49%" align="right">
				<input type="button" id="prev" name="prev" value="'._PREV.'" '.$dis1.' style="width:100px;" onClick="document.getElementById(\'start\').value='.$prev_page.';document.forms[\''.$form_name.'\'].submit();">
				</td>
				<td width="2%" nowrap></td>
				<td width="49%" align="left">
				<input type="button" id="next" name="next" value="'._NEXT.'" '.$dis2.' style="width:100px;" onClick="document.getElementById(\'start\').value='.$next_page.';document.forms[\''.$form_name.'\'].submit();">
				</td>
			</tr>
			</table>
			</div>';

	return $out;
}


function openTable()
{
	$out = '
	<table width="80%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="100%" align="center">';

	echo $out;
}

function CloseTable()
{
	$out = '
		</td>
	</tr>
	</table>';

	echo $out;
}

function createSelect($res, $identifier, $width, $default)
{
	echo '
	<SELECT id="'.$identifier.'" name="'.$identifier.'" size="1" style="width:'.$width.'px">
	<OPTION value="0">'.$default.'</OPTION>';

	while(!$res->EOF)
	{
		if ($res->fields[0] == $_POST[$identifier])
		{
			$sel = 'SELECTED';
		}
		else
		{
			$sel = '';
		}

		echo '<OPTION value="'.$res->fields[0].'" '.$sel.'>'.$res->fields[1].'</OPTION>';

		$res->MoveNext();
	}

	echo '
	</SELECT>';
}

function showInActivity($val, $disabled)
{
	if ($val == 0)
	{
		$sel1 = 'SELECTED';
		$sel2 = '';
	}
	else
	{
		$sel1 = '';
		$sel2 = 'SELECTED';
	}

	$out = '
	<SELECT id="show_record" name="show_record" size="1" style="width:52px;" '.$disabled.'>
	<OPTION value="0" '.$sel1.'>ΟΧΙ</OPTION>
	<OPTION value="1" '.$sel2.'>ΝΑΙ</OPTION>
	</SELECT>';

	return $out;
}

function getImage($img,$tbl)
{
	if (empty($img)) return $out = '';

	$id = trimExtension($img);

	$out = '
	<table border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td class="td9">'.$img.'</td>
	</tr>
	<tr>
		<td align="center"><img id="'.$id.'" src="../media/images/'.$tbl.'/'.$img.'"></td>
	</tr>
	</table>';

	return $out;
}

function getSound($snd,$tbl)
{
	if (empty($snd)) return $out = '';

	$out = '
	<table border="0" cellpadding="2" cellspacing="0">
	<tr>
		<td class="td9">'.$snd.'</td>
	</tr>
	<tr>
		<td align="center">
		<OBJECT classid="clsid:22D6F312-B0F6-11D0-94AB-0080C74C7E95" WIDTH="100" HEIGHT="45">
		<PARAM NAME="autostart" VALUE=false>
		<PARAM NAME="FileName" VALUE="../media/sounds/'.$tbl.'/'.$snd.'">
		<EMBED TYPE="application/x-mplayer2" src="../media/sounds/'.$tbl.'/'.$snd.'" width="100" HEIGHT="45" autostart=0></EMBED>
		</OBJECT>
		</td>
	</tr>
	</table>';

	return $out;
}

function getImageForEI($img,$tbl)
{
	if (empty($img))
	{
		$src = '../images/blank.gif';
	}
	else
	{
		$src = '../../media/images/'.$tbl.'/'.$img;
	}

	$out = '
	<img id="dummy" name="dummy" src="'.$src.'" alt="Image Placeholder">
	<input type="hidden" id="image" name="image" value="'.$img.'">';

	return $out;
}

function getSoundForEI($snd)
{
	$out = '
	<input type="text" id="empty" name="empty" value="'.$snd.'" style="width:250px;" disabled>
	<input type="hidden" id="sound" name="sound" value="'.$snd.'">';

	return $out;
}

function deleteImage()
{
	$out = "deleteImage();";

	return $out;
}

function deleteSound()
{
	$out = "deleteSound();";

	return $out;
}

function uploadPhotoLink($act)
{
	$out = "openWindow('uploadPhoto.php?fld=".$act."',250,170);";

	return $out;
}

function uploadSoundLink($act)
{
	$out = "openWindow('uploadSound.php?fld=".$act."',250,170);";

	return $out;
}

function trimExtension($str)
{
	$pos = strrpos($str,'.');

	$out = substr($str, 0, $pos);

	return $out;
}

function imageResize($img)
{
	$id = trimExtension($img);

	echo '<SCRIPT type="text/javascript">changeImageDimensions(\''.$id.'\');</SCRIPT>';
}

function resizeAllImages($result)
{
	$result->MoveFirst();

	while (!$result->EOF)
	{
		if ($result->fields['image'])
		{
			$value = $result->fields['image'];

			imageResize($value);
		}
		
		$result->MoveNext();
	}
}

function showMultipleImages($images,$tbl)
{
	if (empty($images)) return $out = '';

	$out = '<table>';

	$tmp = explode("\r\n",$images);

	for ($i=0 ; $i < count($tmp); $i++)
	{
		$mod = fmod($i,3);

		if ($mod == 0)
		{
			$out .= '<tr>';
		}

		$out .= '<td>'.getImage($tmp[$i],$tbl).'</td>';

		if ($mod == 2)
		{
			$out .= '</tr>';
		}
	}

	$out .= '</table>';

	return $out;
}

?>