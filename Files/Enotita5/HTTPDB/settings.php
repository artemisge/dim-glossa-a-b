<?php 

error_reporting(E_ALL); # do not echo any errors

if (stristr(PHP_OS, 'WIN')) 
{
    $delimiter = "\\";
}
else 
{
    $delimiter = "/";
}


define('DIR_DELIMITER', $delimiter);
define('FIELD_SEPARATOR', '\t');
define('RECORD_SEPARATOR', '\n');


$logfile = extendDir(dirname(__FILE__), "Logs", "error.log");

define('ADODB_ERROR_LOG_TYPE', 3);
define('ADODB_ERROR_LOG_DEST', $logfile);

include('../teacher/adodb/adodb-errorhandler.inc.php');
include('../teacher/adodb/adodb.inc.php');
include('../teacher/adodb/tohtml.inc.php');


$ADODB_FETCH_MODE = ADODB_FETCH_NUM;


function echoR($arr) 
{
    while (list($key,$val) = each($arr)) 
	{
        if (is_array($val) || is_object($val)) 
		{ 
            echo $key." : <br>";          
            echoR($val);
        }
        else 
		{
            echo"$key -> $val <br>";
        }
    }
} 

function extendDir() 
{
    $args    = array_slice(func_get_args(), 1);
    $basedir = array_slice(func_get_args(), 0, 1);
    $basedir = $basedir[0];

    foreach ($args as $dir) 
	{
        $basedir .= DIR_DELIMITER.$dir;
    }    
    return $basedir;
} 

?>