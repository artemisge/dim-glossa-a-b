function fix_str_len(str, len) {
	if (str.length >= len) {
		return str.substring(0, len);
	} else {
		var d = len - str.length;
		var l = Math.floor(d / 2);
		var r = len - l - str.length;
		return (chr_rpt(" ", l) + str.substring(0, len) + chr_rpt(" ", r));
	}
}
function chr_rpt(chr, tms) {
	if (tms <= 0) {
		return "";
	}
	var res = "";
	while (tms--) {
		res += chr;
	}
	return res;
}
function chr_del(str, chr) {
	var res = "";
	var i = 0, j = str.length, c;
	while (i < j) {
		if ((c = str.charAt(i++)) != chr) {
			res += c;
		}
	}
	return res;
}
function chr_lim_it(str, chr, lim) {
	//returns the str string with limited number of chr
	//e.g. chr_lim_it('qwer    ty', " ", 1); will return 'qwer ty'
	var res = "", b = 0;
	var i = 0, j = str.length, c;
	while (i < j) {
		c = str.charAt(i);
		if (c == chr) {
			if ((b++) < lim) {
				res += c;
			}
		} else {
			b = 0;
			res += c;
		}
		i++;
	}
	return res;
}
