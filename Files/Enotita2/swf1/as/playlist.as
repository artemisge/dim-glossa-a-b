_global.playlist = function(engine, list) 
{
	this.engine = engine;
	this.list = list;
	this.handler = handler;
	this.object = (obj == undefined) ? _root : obj;
	this.counter = 0;
	
	this.proceed = function(arg, forced) 
	{
		if (!forced) 
		{
			if (this.counter == this.list.length) 
			{
				this.onDone();
			} 
			else 
			{
				SOUND_VOX.doLoad(this.list[this.counter++], true, true);
				SOUND_VOX.handler = "proceed";
				SOUND_VOX.handlerObject = this;
			}
		}

		this.__callBack(arg, forced);
	}

	this.__callBack = function(arg, forced) 
	{
		if (typeof this.handler == "string") 
		{
			this.object[this.handler](arg, forced);
		} 
		else 
		{
			this.handler(arg, forced);
		}
		
		this.onCallBack(arg, forced, this.counter - 1);
	}

	this.proceed();
}
