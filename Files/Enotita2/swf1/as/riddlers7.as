/*���������� ������ ����� 1:
  ���� �� ����������� - startDrag ���� ����� �������
  ������ ��������, ������ ��������� ������������ ���� ������ ��� ����.*/
function new_riddle(alim) {
	_global.riddle = new Object();
	_root.update_ok = false;
	riddle.words = [];
	riddle.places = [];
	riddle.taken = [];
	riddle.picked = undefined;
	riddle.refresh = undefined;
	riddle.limit = alim;
	riddle.found = 0;
	riddle.errcount = 0;
	riddle.mouse = new Object();
	riddle.mouse.onMouseUp = function() {
		if (!riddle.waiting) {
			switch (riddle.picked == undefined) {
			case true :
				var i = 0;
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip._alpha > 0) {
						if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
							riddle.picked = riddle.words[i];
							riddle.picked.clip.swapDepths(1000);
							riddle.refresh = setInterval(refresh_word, 5);
							sfx.play_sound(sfx.pick);
							_root.stop_solution();
							break;
						}
					}
				}
				break;
			case false :
				var i = 0;
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip._alpha > 0) {
						if (riddle.words[i].id != riddle.picked.id) {
							if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
								riddle.picked.clip._x = riddle.picked.origin_x;
								riddle.picked.clip._y = riddle.picked.origin_y;
								riddle.picked = riddle.words[i];
								riddle.picked.clip.swapDepths(1000);
								riddle.refresh = setInterval(refresh_word, 5);
								sfx.play_sound(sfx.pick);
								break;
							}
						}
					}
				}
				var i = 0;
				var found = false;
				var test_clip;
				for (i; i < riddle.places.length; i++) {
					if (riddle.picked.master_clip == undefined) {
						test_clip = riddle.places[i].clip;
					} else {
						test_clip = riddle.picked.master_clip;
					}
					if (test_clip.hitTest(_root._xmouse, _root._ymouse, false)) {
						riddle.refresh = clearInterval(riddle.refresh);
						riddle.refresh = undefined;
						if (riddle.places[i].id == riddle.picked.id && riddle.places[i] !== riddle.picked && (riddle.picked.master_clip == undefined || riddle.picked.master_clip._alpha > 0)) {
							if (riddle.taken[riddle.places[i].place] == 0) {
								riddle.done = 0;								
								_root.stop_solution();
								riddle.taken[riddle.places[i].place]++;
								found = true;
								riddle.places[i].clip._alpha = 100;
								riddle.picked.clip._alpha = 0;
								riddle.errcount = 0;
								_root.riddler.msave();
								sfx.play_sound(sfx.place_ok);
								if (++riddle.found == riddle.limit) {
									_root.riddle_done(true);
								}
							}
						}
						//	riddle.picked = undefined;
						//	break;
					}
				}
				if (!found) {
					riddle.picked.clip._x = riddle.picked.origin_x;
					riddle.picked.clip._y = riddle.picked.origin_y;
					sfx.play_sound(sfx.place_wrong);
					if (++riddle.errcount == 3) {
						riddle.errcount = 0;
						_root.give_help();
					}
				}
				delete riddle.picked;
				break;
			}
		}
	};
	Mouse.addListener(riddle.mouse);
}
function solve_riddle() {
	if (riddle.picked != undefined) {
		riddle.picked.clip._x = riddle.picked.origin_x;
		riddle.picked.clip._y = riddle.picked.origin_y;
		riddle.picked = undefined;
	}
	var i = 0;
	for (i; i < riddle.places.length; i++) {
		if (riddle.taken[riddle.places[i].place] == 0) {
			riddle.taken[riddle.places[i].place]++;
			riddle.words[i].clip._alpha = 0;
			riddle.places[i].clip._alpha = 100;
			riddle.found++;
		}
	}
	_root.riddle_done();
}
function refresh_word() {
	riddle.picked.clip._x = _xmouse;
	riddle.picked.clip._y = _ymouse;
	if (_root.update_OK) {
		updateAfterEvent();
	}
}
function add_word(aclip, aid, aMstr) {
	var w = new item(aclip, aid);
	w.master_clip = aMstr;
	//trace(w.master_clip)
	riddle.words.push(w);
}
function add_place(aclip, aid, aPlace) {
	var p = new item(aclip, aid);
	p.place = aPlace;
	riddle.taken[aPlace] = 0;
	riddle.places.push(p);
	aclip._alpha = 0;
	if (riddle.places.length == riddle.limit) {
		_root.update_OK = true;
	}
}
function item(aclip, aid) {
	this.clip = aclip;
	this.origin_x = aclip._x;
	this.origin_y = aclip._y;
	this.id = aid;
}