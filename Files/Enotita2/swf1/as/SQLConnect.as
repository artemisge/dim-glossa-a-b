
/*

*################################################################################*
##				   SQL Connection by Bournazos Charilaos 2004	  				##
*################################################################################*
##																				##
##	You have to put HTTPDB into your site or wherever u like.					##
##	Insert the action.php and settings.php files into the HTTPDB folder			##
##	then you'll have to create the sqlSettings initializer:						##
##																				##
##		_global.Settings = new sqlSettings(										##
##											"HTTPDB/action.php",				##
##											"http://www.yourURL.com/",			##
##											"localhost",						##
##											"database_username",				##
##											"database_password",				##
##											"database_name",					##
##											"sql_respond",						##
##											"_root",							##
##											"my_parser"							##
##											);									##
##																				##
##	sqlSettings.parser (last option) is optional and defines a user function	##
##	in _root object, to be used for parsing sql returned data.					##
##																				##
##	then initialize the SQL connection:											##
##																				##
##		mySQL = new SQL(Settings);												##
##																				##
##	Now you are ready to pass any query like:									##
##																				##
##		mySQL.execQuery("select * from <table_name>");							##
##																				##
##		After a query is finished you'll catch the returned data formated into	##
##	the user-defined response handler.											##
##		Response handler recieves to arguments:									##
##			1)	the data returned												##
##			2)	the Reason that query has been executed							##
##																				##
##	e.g.																		##
##																				##
##		function sql_respond (data, reason) {									##
##			switch (reason) {													##
##				.																##
##				trace(data[0][1]);												##
##				.																##
##				.																##
##			}																	##
##		}																		##
##																				##
*################################################################################*

*/

//SQL class, initializes with an sqlSettings structure
function SQL(sqlSettings){
	this.parser = (sqlSettings.parser == undefined) ? "sql_default_parse" : sqlSettings.parser;
	this.port = sqlSettings.port;
	this.server = new Object();
	this.server.url = sqlSettings.url;
	this.server.host = sqlSettings.host;
	this.server.username = sqlSettings.username;
	this.server.password = sqlSettings.password;
	this.database = sqlSettings.database;
	this.responseHandler = sqlSettings.responseHandler;
	this.responseObject = sqlSettings.responseObject;
	_global.reasons = []
}

/*
executes the query and pushes the aReason into the reasons array
aReason is used as an id near to the user to understand
wich action caused responce
*/
SQL.prototype.execQuery = function(Query, aReason) {
	var loader = new LoadVars();
	loader.parent = this;
	loader.reason = aReason;
	loader.onLoad = function(success) {
		var res = unescape((this.toString()).split("=&")[0])
		if (res.indexOf("onLoad") == 0) {
			res = undefined;
		}else{
			res = _root[this.parent.parser](res);
		}
		var reason = reasons[reasons.length - 1];
		_global.reasons.pop();
		eval(this.parent.responseObject)[this.parent.responseHandler](res, this.reason);
		delete this;
	};
	_global.reasons.push(aReason);
	loader.load(this.port + "?host=" + this.server.host + "&username=" + this.server.username + "&password=" + this.server.password + "&db=" + this.database + "&query=" + escape(Query));
};

/*
formats the sql results
delimiters: \n -> new record
			\t -> new property
*/
_root.sql_default_parse = function(txt) {
	var tmp = txt.split("\\n");
	var ares = [];
	var i = 0;
	var j = tmp.length;
	while (i < j) {
		if (tmp[i] != "") {
			ares[i] = tmp[i].split("\\t");
		}
		i++;
	}
	return ares;
}

//sqlSettings structure
function sqlSettings(phpHandler, url, host, username, password, database, resHandler, resObj, parser) {
	this.port = phpHandler;
	this.url = url;
	this.host = host;
	this.username =username;
	this.password = password;
	this.database = database;
	this.responseHandler = resHandler;
	this.responseObject = resObj;
	this.parser = parser;
}
