//l_rect defines an object with a movie clip wich uses one line and one corner movie clip
//to display any rect user wants.
//x: Number, the left value of the rectangle
//y: Number, the top value of the rectangle
//w: Number, width of the rectangle
//h: Number, height of the rectangle
//z: Number, the depth of the movie clip wich is about to be created
//p: MC Instance, the movie clip that will be parent of the rectangle (default: _root)
//bg: Linkage, optional background mc
_global.rectangle = function(x, y, w, h, z, p, bg) {
	if (p == undefined) {
		p = _root;
	}
	this.mc = p.createEmptyMovieClip("rect_" + z, z);
	if (bg != undefined) {
		this.mc.attachMovie(bg, "bg", 0);
	}
	this.x = x;
	this.y = y;
	this.width = w;
	this.height = h;
	this.parent = p;
	this.mc.attachMovie(RECT_T_LINE, "top", 1);
	this.mc.attachMovie(RECT_R_LINE, "right", 2);
	this.mc.attachMovie(RECT_B_LINE, "bottom", 3);
	this.mc.attachMovie(RECT_L_LINE, "left", 4);
	this.mc.attachMovie(RECT_TL_CORNER_WIRE, "tl", 5);
	this.mc.attachMovie(RECT_TR_CORNER_WIRE, "tr", 6);
	this.mc.attachMovie(RECT_BR_CORNER_WIRE, "br", 7);
	this.mc.attachMovie(RECT_BL_CORNER_WIRE, "bl", 8);
	//this.mc.bg._alpha = 40;
	this.redraw();
}
_global.rectangle.prototype.redraw = function() {
	this.mc._x = this.x;
	this.mc._y = this.y;
	this.mc.bg._x = this.mc.bg._y = 2.6;
	this.mc.bg._width = this.width-2;
	this.mc.bg._height = this.height-2;
	this.mc.top._width = this.width;
	this.mc.bottom._width = this.width;
	this.mc.right._height = this.height;
	this.mc.left._height = this.height;
	this.mc.right._y = 4;
	this.mc.left._y = 4;
	this.mc.left._x = 0;
	this.mc.top._x = 4;
	this.mc.bottom._x = 4;
	this.mc.right._x = this.width + 8;
	this.mc.bottom._y = this.height + 8;
	//this.mc.tr._rotation = 90;
	this.mc.tr._x = this.width + 8;
	//this.mc.br._rotation = 180;
	this.mc.br._x = this.width + 8;
	this.mc.br._y = this.height + 8;
	//this.mc.bl._rotation = 270;
	this.mc.bl._y = this.height + 8;
}
