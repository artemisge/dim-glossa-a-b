_global.DUMMY_MC = "DUMMY_MC";
_global.MENU_ITEM = "MENU_ITEM_MC";
//rectangle class components definitions
_global.RECT_T_LINE = "RECT_T_LINE";
_global.RECT_B_LINE = "RECT_B_LINE";
_global.RECT_L_LINE = "RECT_L_LINE";
_global.RECT_R_LINE = "RECT_R_LINE";
_global.RECT_TL_CORNER_WIRE = "RECT_TL_CORNER_WIRE";
_global.RECT_TR_CORNER_WIRE = "RECT_TR_CORNER_WIRE";
_global.RECT_BR_CORNER_WIRE = "RECT_BR_CORNER_WIRE";
_global.RECT_BL_CORNER_WIRE = "RECT_BL_CORNER_WIRE";
