function popup(parent, idName, pu_type, script, handler_object, handler, az, ax, ay) {	
	this.parent = (parent == undefined) ? _root : parent;
	this.name = idName;
	this.type = pu_type;
	this.script = script;
	this.x = ax;
	this.y = ay;
	this.handler_obj = handler_object;
	if (handler_object == undefined) {
		this.handler_obj = _root;
	}
	this.handler = "generic_popup_onClick";
	if (handler != undefined) {
		this.handler = handler;
	}
	this.z = 10;
	if (az != undefined) {
		this.z = az;
	}
	this.build();
}
popup.prototype.build = function() {
	var dta = this.script;
	this.mc = this.parent.attachMovie(DUMMY_MC, this.name, this.z, {_x:this.x, _y:this.y});
	this.mc.class = this;
	this.mc._alpha = 0;
	this.border = new rectangle(0, 0, 50, 50, 1, this.mc);
	this.mc.onEnterFrame = function() {
		if (this.class.state == "open") {
			if ((this._alpha += 25) >= 100) {
				this._alpha = 100;
				this.class.state = "idle";
			}			
		} else if (this.class.state == "idle") {
			if ((!this.hitTest(_xmouse, _ymouse) && (this.class.lastRoll.subMenu == undefined) && (this.class != this.class.parent.lastRoll.subMenu))) {
				this.class.state = "close";
			}
		} else if (this.class.state == "close") {
			if (this._alpha == 100) {
				this.class.handler_obj.onRollOut(true);
			}
			if ((this._alpha -= 25) <= 0) {
				this.class.suicide();
			}
		}
	};
	this.items = [];
	var i = 0, j = dta.length, itm, linkID, ch = 0;
	var subScr, capt;
	while (i < j) {
		if ((typeof dta[i]) == "object") {
			capt = dta[i][0];
			subScr = dta[i].slice(1, dta[i].length);
		} else{
			capt = dta[i];
			subScr = undefined;
		}
		this.items.push(itm = this.mc.attachMovie(MENU_ITEM, "item_" + i, i + 2, {_id:ch, _caption:capt, script: subScr}));
		itm._x = 3;
		itm.onRelease = function() {
			if (this.subMenu == undefined) {
				var res = ""
				var testItem = this
				while (testItem._parent.lastRoll._id != undefined) {
					res = testItem._parent.lastRoll._id + ((res.length) ? "_" : "") + res;
					testItem = testItem._parent
				}
				testItem.class.select(res);
			}
		};
		if (capt == "/--") {
			itm.caption._visible = false;
			itm.enabled = itm.useHandCursor = false;
			itm._height /= 2;
			itm.bg.gotoAndStop(3);
		} else {
			ch++;
		}
		if (i == 0) {
			itm._y = 3;
		} else {
			itm._y = this.items[i - 1]._y + this.items[i - 1]._height - 1;
		}
		
		i++;
		
	}
	this.border.height = itm._y + itm._height - 4;
	if (this.type == 1) {
		this.y = this.mc._y -= itm._y + (2 * itm._height);
	}
	this.state = "open";
};
popup.prototype.arrange = function() {
	var i = 0, j = this.items.length, itm, max_width;
	var aw;
	while (i < j) {
		aw = this.items[i].caption._width;
		if (this.items[i].script != undefined) {
			aw += this.items[i].sub_arr._width + 5;
		}
		if (aw > max_width) {
			max_width = aw + 5;
		}
		i++;
	}
	i = 0;
	while (i < j) {
		this.items[i++].setSize(max_width + 5);
	}
	this.border.width = max_width + 3;
	this.border.redraw();
};
popup.prototype.select = function(id) {
	this.handler_obj[this.handler](id);
	this.state = "close";
};
popup.prototype.suicide = function() {
	this.mc.removeMovieClip();
	delete this;
};
//loader only supports 1 depth menus (one submenu)
function PU_load_file(mnu, obj, hnd) {
	if (obj == undefined) {
		obj = _root;
	}
	var l = new LoadVars();
	l.obj = obj;
	l.hnd = hnd;
	l.load(mnu);
	l.onLoad = function(success) {
		if (!success) {
			trace("file not found or wrong filetype.");
		} else {
			//_global.MAIN_POPUP_SCRIPT = 
			if (this.hnd == undefined) {
				_root.PU_File_Loaded(parse_menu(unescape(this).split("=&")[0].split("\r\n")));
			} else {
				this.hnd(parse_menu(unescape(this).split("=&")[0].split("\r\n")));
			}
		}
	};
}
function parse_menu(lines) {
	var deg = [];
	var res = [];
	var i = 0, j = lines.length;
	var d = 0;
	while (i < j) {
		deg[i] = lines[i].split("\t").length - 1;
		i++;
	}
	var sline = 0;
	var del_lines = 0;
	i = 0;
	while (i < j) {
		if ((deg[i + 1] != undefined) && (deg[i + 1] < deg[i])) {
			var ds = sline - del_lines;
			var gr = parse_menu(lines.slice(sline, i));
			res.splice(res.length - gr.length, gr.length);
			res.push(gr);
			del_lines += i - ds;
		} else {
			if (deg[i + 1] > deg[i]) {
				sline = i;
			}
			res.push(lines[i].substring(deg[i], lines[i].length));
		}
		i++;
	}
	if (isNaN(res[res.length - 1].charCodeAt(0))) {
		res.pop();
	}
	return res;
}
