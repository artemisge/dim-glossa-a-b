function new_riddle(alim) {
	_global.riddle = new Object();
	_root.update_ok = false;
	riddle.words = [];
	riddle.places = [];
	riddle.picked = undefined;
	riddle.refresh = undefined;
	riddle.limit = alim;
	riddle.found = 0;
	riddle.errcount = 0;
	riddle.mouse = new Object();
	riddle.mouse.onMouseUp = function() {
		if (!riddle.waiting) {
			switch (riddle.picked == undefined) {
			case true :
				var i = 0;			
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
	//					var ps = {_x: _root._xmouse, _y: _root._ymouse};
						riddle.picked = new Item(attachMovie("w" + _root.next.current_level + "_" + riddle.words[i].id, "sw" + riddle.words[i].id, riddle.found + 1), riddle.words[i].id,  {_x: _xmouse, _y: _ymouse});
						riddle.picked.clip.swapDepths(1000);
	//					refresh_word();
						riddle.refresh = setInterval(refresh_word, 5);
							sfx.play_sound(sfx.pick);
						break;
					}
					//}
				}
				break;
			case false :
				removeMovieClip(riddle.picked.clip);
				var i = 0;
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip._alpha > 0) {
						if (riddle.words[i].id != riddle.picked.id) {
							if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {			
	//							var ps = {_x: _root._xmouse, _y: _root._ymouse};
								riddle.picked = new Item(attachMovie("w" + _root.next.current_level + "_" + i, "sw" + i, riddle.found + 1), riddle.words[i].id, ps,  {_x: _xmouse, _y: _ymouse});
								riddle.picked.clip.swapDepths(1000);
								//riddle.refresh = setInterval(refresh_word, 5);
								sfx.play_sound(sfx.pick);
								break;
							}
						}
					}
				}
				var i = 0;
				var found = -1;
				for (i; i < riddle.places.length; i++) {
					if (riddle.places[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
						if ((riddle.places[i].clip._alpha == 0 || riddle.places[i].clip === _root.help_clip) && riddle.places[i].id == riddle.picked.id) {
							_root.stop_solution();
							riddle.errcount = 0;
							found = i;
						}
					} else {
					}
				}
				if (found == -1) {
					riddle.picked.clip.useHandCursor = false;
					sfx.play_sound(sfx.place_wrong);
					if (++riddle.errcount == 3) {
						_root.give_help();
						riddle.errcount = 0;
					}
				} else {
					riddle.places[found].clip._alpha = 100;
					riddle.picked.clip._alpha = 0;
					sfx.play_sound(sfx.place_ok);
					if (++riddle.found == riddle.limit) {
						_root.riddle_done();
					}
				}
				clearInterval(riddle.refresh);
				riddle.refresh = undefined;
				riddle.picked = undefined;
				break;
			}
			//_root.update_ok = old_update;
		}
	};
	Mouse.addListener(riddle.mouse);
}
function solve_riddle() {
	if (riddle.picked != undefined) {
		removeMovieClip(riddle.picked.clip);
	}
	var i = 0;
	for (i; i < riddle.places.length; i++) {
		riddle.places[i].clip._alpha = 100;
	}
	var i = 0;
	for (i; i < riddle.words.length; i++) {
		_root.cursor.unregister_item(riddle.words[i].clip);
	}
	Mouse.removeListener(riddle.mouse);
	riddle.found = riddle.limit;
	riddle.picked = undefined;
	_root.riddle_done();
}
function refresh_word() {
	riddle.picked.clip._x = _xmouse;
	riddle.picked.clip._y = _ymouse;
	if (_root.update_OK) {
		updateAfterEvent();
	}
}
function add_word(aclip, aid) {
	var w = new item(aclip, aid);
	riddle.words.push(w);
}
function add_place(aclip, aid) {
	var p = new item(aclip, aid);
	riddle.places.push(p);
	aclip._alpha = 0;
	if (riddle.places.length == riddle.limit) {
		_root.update_OK = true
	}
}
function item(aclip, aid) {
	this.clip = aclip;
	this.origin_x = aclip._x;
	this.origin_y = aclip._y;
	this.id = aid;
}
