#include "../../as/adds/array.as"
#include "../../as/chrlib.as"
#include "../../as/booleans.as"

function fDB(loc) {
	_global.fDB_Error = undefined;
	this.tables = [];
	this.queue = [];
	this.args = [];
	if (loc != undefined) {
		this.load(loc);
	}
}
_global.fDB_Err = function(err) {
	if (err == undefined) {
		return fDB_Error;
	} else {
		_global.fDB_Error = err;
	}
};
fDB_table = function (parent, name) {
	this.parent = parent;
	this.name = name.toUpperCase();
	this.props = [];
	this.recs = [];
	this.count = function() {
		return this.recs.length;
	}
	this.addProp = function(name) {
		this.props.push(name.toUpperCase());
	};
	this.addRec = function(rec) {
		this.recs.push(rec);
	};
	this.select = function(props, crit) {
		var idx = [];
		var i = 0, ls;
		var res = [];
		if (props[0] == "*") {
			var j = this.props.length;
			while (i < j) {
				idx.push(i++);
			}
		} else {
			var j = props.length;
			while (i < j) {
				idx.push(ls = this.props.getPos(props[i++].toUpperCase()));
				if (ls == undefined) {
					fDB_Err("couldn't find property '" + props[i - 1] + "'.");
					return -1;
				}
			}
		}
		/*if (crit.indexOf("=") > -1) {
			var oper = "=";
		} else {
			var oper = "<>";
		}
		var cProp = this.props.getPos(crit.substring(0, crit.indexOf(oper)));
		var cVal = crit.substring(crit.lastIndexOf(oper) + 1, crit.length);*/
		var i = 0, j = this.recs.length, k, l = idx.length, r = -1;
		while (i < j) {
			//if (this.recs[i][cProp] == cVal) {
			if (this.parent.calc_bool(crit, this.props, this.recs[i]) || (crit == undefined)) {
				k = 0;
				res.push([]);
				r++;
				while (k < l) {
					res[r].push(this.recs[i][idx[k]]);
					k++;
				}
			}
			i++;
		}
		return res;
	};
	this.toString = function(len) {
		if (len == undefined) {
			len = 10;
		} else {
			len = parseInt(len);
		}
		var res = "table: " + this.name + "\n" + "\n";
		var i = 0, j = this.props.length;
		while (i < j) {
			res += " " + fix_str_len(this.props[i++], len);
		}
		res += "\n" + chr_rpt("-", (len + 1) * this.props.length) + "\n";
		var i = 0, j = this.recs.length;
		var l, k = this.recs[0].length;
		while (i < j) {
			l = 0;
			while (l < k) {
				res += " " + fix_str_len(this.recs[i][l], len);
				l++;
			}
			res += "\n";
			i++;
		}
		return res;
	};
};
fDB.prototype.onData_loaded = function(dta, reason, args) {
	switch (reason) {
	case "index load" :
		var tabNames = dta.split("\r\n");
		var i = 0, j = tabNames.length, nm;
		this.tables_loading = j;
		while (i < j) {
			this.tables.push(new fDB_table(this, nm = tabNames[i].substring(0, tabNames[i].lastIndexOf("."))));
			this.load_data(this.path + "/" + tabNames[i], "table load", nm);
			i++;
		}
		break;
	case "table load" :
		var recs = dta.split("\r\n");
		var tblname = chr_del(recs.shift(), "\t");
		var header = recs.shift().split("\t");
		var i = 0, j = header.length;
		var tbl = this.table_by_name(tblname);
		while (i < j) {
			if (header[i].length) {
				tbl.addProp(header[i]);
			}
			i++;
		}
		i = 0, j = recs.length;
		while (i < j) {
			if (recs[i].length) {
				tbl.addRec(recs[i].split("\t"));
			}
			i++;
		}
		if ((--this.tables_loading) == 0) {
			this.onLoaded();
		}
		//trace(tbl.toString(15));
		break;
	}
};
fDB.prototype.load_data = function(src, reason) {
	this.queue.push(reason);
	this.loader = new LoadVars();
	this.loader.parent = this;
	this.loader.onLoad = function() {
		this.parent.onData_loaded(unescape(this).split("=&")[0], this.parent.queue.pop(), unescape(this).split("=&")[1]);
	};
	this.loader.load(src);
};
fDB.prototype.load_tables = function(index) {
};
fDB.prototype.load = function(location) {
	this.path = location;
	this.load_data(location + "/index.idx", "index load");
};
fDB.prototype.table_by_name = function(name) {
	var i = 0, j = this.tables.length;
	while (i < j) {
		if (this.tables[i++].name.toUpperCase() == name.toUpperCase()) {
			return this.tables[i - 1];
		}
	}
	return -1;
};
fDB.prototype.exec = function(que) {
	que = que.toUpperCase();
	var dta = que.split(" ")
	var com = dta[0];
	if (com == "SELECT") {
		var props = chr_del(que.substring(6, que.indexOf(" FROM ")), " ");
		var hasCrit = (que.indexOf(" WHERE "));
		var table = chr_del(que.substring(que.lastIndexOf("FROM ") + 5, (hasCrit > -1) ? hasCrit : que.length), " ");
		if (hasCrit > -1) {
			var criteria = chr_lim_it(que.substring(que.lastIndexOf("WHERE ") + 6, que.length), " ", 1);
		}
		return this.table_by_name(table).select(props.split(","), criteria);
	} else if (com == "COUNT") {
		var table = chr_del(dta[1], " ");
		return this.table_by_name(table).count();
	}
};
fDB.prototype.calc_bool = function(str, idx, rec) {
	//str = chr_lim_it(str, " ", 1);
	var i = 0, j = str.length;
	var s = "", c;
	while (i < j) {
		c = str.charAt(i);
		switch (c) {
		case "(" :
			s += "( ";
			break;
		case ")" :
			s += " )";
			break;
		case "<" :
			s += "< ";
			break;
		case ">" :
			s += " >";
			break;
		default :
			s += c;
		}
		i++;
	}
	str = s.split("&").join(" & ");
	str = s.split("|").join(" | ");
	str = s.split("=").join(" = ");
	str = s;
	str = chr_lim_it(str, " ", 1);
	var dta = str.split(" ");
	log = ["&", "|"];
	ops = ["=", "<>", ">", "<"];
	//first read all needed props
	var i = 0, j = dta.length, t;
	while (i < j) {
		if (log.getPos(dta[i]) == undefined) {
			if (fDB_isStr(dta[i])) {
				dta[i] = dta[i].substring(1, dta[i].length - 1).toUpperCase();
			} else if (fDB_isNum(dta[i])) {
				dta[i] = parseInt(dta[i]);
			} else {
				if (ops.getPos(dta[i]) == undefined) {
					dta[i] = rec[idx.getPos(dta[i])].toUpperCase();
				}
			}
		}
		i++;
	}
	i = 0, j = dta.length;
	var r = "", t, b;
	while (i < j) {
		if ((t = ops.getPos(dta[i])) != undefined) {
			switch (ops[t]) {
			case "=" :
				b = (dta[i - 1] == dta[i + 1]);
				break;
			case "<>" :
				b = (dta[i - 1] != dta[i + 1]);
				break;
			case ">" :
				b = (dta[i - 1] > dta[i + 1]);
				break;
			case "<" :
				b = (dta[i - 1] < dta[i + 1]);
				break;
			}
			r += b + " ";
		} else if (log.getPos(dta[i]) != undefined) {
			r += dta[i] + " ";
		}
		i++;
	}
	return calculate_booleans(r);
};
_global.fDB_isStr = function(str) {
	return ((str.charAt(0) == "'") && (str.charAt(str.length - 1) == "'"));
}
_global.fDB_isNum = function(str) {
	return !isNaN(parseInt(str));
}
