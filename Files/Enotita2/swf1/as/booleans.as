_global.calculate_booleans = function(str) {
	var stack = [];
	var res;
	var buffer = "";
	var i = 0;
	var char;
	for (i; i < str.length; i++) {
		char = str.charAt(i);
		if (char == "(") {
			stack.push(buffer);
			buffer = "";
		} else if ((char == ")") || (i == str.length - 1) && (stack.length > 0)) {
			buffer = stack.pop() + parse_bool(buffer);
		} else {
			buffer += char;
		}
	}
	return parse_bool(buffer);
}
_global.parse_bool = function(str) {
	var res = 0;
	var char;
	var buffer = "";
	var lastAct;
	var i = 0;
	for (i; i < str.length; i++) {
		char = str.charAt(i);
		if (char == "!" || char == "&" || char == "|" || i == str.length - 1) {
			if (i == str.length - 1) {
				buffer += char;
			}
			if (buffer == "!") {
				buffer = ""
			}else{
				switch (lastAct) {
				case "!" :
					var t = Bool(buffer);
					if (res == undefined) {
						res = !t;
					} else {
						res = res and !t;
					}
					buffer = "";
					break;
				case "&" :
					res = res and Bool(buffer);
					buffer = "";
					break;
				case "|" :
					res = res or Bool(buffer);
					buffer = "";
					break;
				default :
					res = Bool(buffer);
					buffer = "";
					break;
				}
				lastAct = char;
			}
		} else {
			if (char != " ") {
				buffer += char;
			}
		}
	}
	return Bool(res);
}
_global.Bool = function(str) {
	if (str.substring(0, 4) == "true") {
		return true;
	} else if (str.substring(0, 5) == "false") {
		return false;
	} else if (str == 0 || str == "0") {
		return false;
	} else if (str > 0 || str > 0) {
		return true;
	} else {
		var ii;
		if ((ii = get_var(str)) != undefined) {
			return Bool(variables[ii].value);
		}
	}
}
