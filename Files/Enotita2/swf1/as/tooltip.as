///////////////////////////////////////////////////////////////////
//ToolTip behaviour implemantation in MovieClip predefined object//
//					Bournazos Charilaos 2005					 //
//###############################################################//
//---------------------------------------------------------------//
//Usage:	  <instance>._tooltip = <tooltiptext>;				 //
//            <instance>: the instance name of the movieclip     //
//			  <tooltiptext>: the text of the tooltip to be shown //
//---------------------------------------------------------------//
//Limitation: You need to have a movieclip in the library with   //
//			  linkage ID: "tooltip". This mc should contain a    //
//			  dynamic text with instance name: "caption"         //
//---------------------------------------------------------------//
//Tip:		  In the first frame of the "tooltip" movieclip put  //
//			  this code:										 //
//						caption.autoSize = "center";			 //
///////////////////////////////////////////////////////////////////

/*
initialize the setToolTip method
passing something in txt var creates the tooltip
passing "" string in txt var disables the tooltip
*/
#include "./as/point.as"

MovieClip.prototype.setToolTip = function(txt) {
	if (txt == "") {
		clearInterval(this.tooltipdata.check);
		if (this.tooltipdata.mc.owner == this) {
			this.tooltipdata.mc.removeMovieClip();
		}
		delete this.tooltipdata;
	} else {
		if (this.tooltipdata == undefined) {
			this.tooltipdata = new Object();
			this.tooltipdata.mc = this.attachMovie("tooltip", "ttipmc", 10000, {_visible:false, owner: this});
		}
		this.tooltipdata.text = txt;
		this.tooltipdata.mc.caption.text = txt;
		this.tooltipdata.enabled = true;
		this.tooltipdata.check = setInterval(this, "checktooltip", 1);
	}
};
MovieClip.prototype.getToolTip = function() {
	return this.tooltipdata.text;
}
/*
Toggles the tooltip (On / Off)
*/
MovieClip.prototype.toggleToolTip = function() {
	this.tooltipdata.enabled = !this.tooltipdata.enabled;
}
/*
checking out for rollovers of the MovieClip,
it's being called by the 'this.tooltipdata.check' interval
only if the tooltiptext is different than "".
*/
MovieClip.prototype.checktooltip = function(force) {
	if ((this.tooltipdata.enabled || force) && this.hitTest(_xmouse, _ymouse, true) && this._visible) {
		if (!this.tooltipdata.mc) {
			this.tooltipdata.mc = _root.attachMovie("tooltip", "ttipmc", 10000, {_visible:false, owner: this});
			var b = this.getBounds();
			var p = new Object();
			if (this.tooltipdata.xPos == undefined) {
				p.x = b.xMin + ((this._width / 2) / (this._xscale / 100));
			} else {
				p.x = this.tooltipdata.xPos;
			}
			if (this.tooltipdata.yPos == undefined) {
				p.y = b.yMin - ((this.tooltipdata.mc._height/2) / (this._yscale / 100)) - 5;
			} else {
				p.y = this.tooltipdata.yPos;
			}
			this.localToGlobal(p);
			this.tooltipdata.mc._x = p.x;
			this.tooltipdata.mc._y = Math.max(35, p.y);
		} else {
			this.tooltipdata.mc.owner = this;
			this.tooltipdata.mc._visible = true;
		}
		this.tooltipdata.mc.caption.text = this.tooltipdata.text;
		this.tooltipdata.mc.gfx._width = this.tooltipdata.mc.caption.textWidth + 15;
	} else {
		if (this.tooltipdata.mc) {
			if (this.tooltipdata.mc.owner == this) {
				this.tooltipdata.mc.removeMovieClip();
				this.tooltipdata.mc.caption.text = "";
				this.tooltipdata.mc._x = this.tooltipdata.mc._y = 0;
				this.tooltipdata.mc._visible = false;
			}
			this.tooltipdata.mc = undefined;
		}
	}
};

if (!MovieClip.prototype.addProperty("_tooltip", this.getToolTip, this.setToolTip)) {
	trace("��� ������� �� ����������� �� _tooltip ��� movieClip.");
}

//removes the last shown tooltip
_global.killToolTip = function() {
	_root.ttipmc.removeMovieClip();
}
