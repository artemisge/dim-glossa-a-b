/*
///////////////////////////////////////////////////////////////////
//Cursor behaviour implemantation in MovieClip predefined object//
//					Bournazos Charilaos 2005					 //
//###############################################################//
//---------------------------------------------------------------//
//Usage:	  <instance>._cursor = <cursor_frame>;				 //
//            <instance>: the instance name of the movieclip     //
//			  <cursor_frame>: the frame that shows the wanted    //
//							  cursor, inside the cursor movieClip//
//---------------------------------------------------------------//
//Limitation: You need to have a movieclip in the library with   //
//			  linkage ID: "GLOBAL_CURSOR". This mc should		 //
//			  one cursor in each frame and stop() action		 //
//			  on frame 1.										 //
///////////////////////////////////////////////////////////////////
default cursors:
	frame 1: arrow
	frame 2: finger
	frame 3: wait
*/
#include "./as/adds/array.as"
_global.cursor_mc = _root.attachMovie("GLOBAL_CURSOR", "Global_Cursor", 99999999);
Mouse.hide();
MovieClip.prototype.getCursor = function(){
	return this.cursor_id;
}
MovieClip.prototype.setCursor = function(id){
	if (typeof id == "string") {
		id = parseInt(id);
		this.enable_on_pause = true;
	}
	if (id == -1) {
		delete this.cursor_id
		clearInterval(this.cursor_int);
		delete this.cursor_int;
		delete this.enable_on_pause;
		var p;
		if ((p = cursor_registered_items.getPos(this)) != undefined) {
			cursor_registered_items.deleteAt(p);
		}
	} else {
		this.cursor_id = id;
		/*if (this.cursor_int == undefined) {
			this.cursor_int = setInterval(this, "check_cursor", 1);				
		}*/
		if (cursor_registered_items.getPos(this) == undefined) {
			_global.cursor_registered_items.push(this);
			//_global.cursor_registered_items.sort(sort_by_depth);
		}
	}
}
if (!MovieClip.prototype.addProperty("_cursor", MovieClip.prototype.getCursor, MovieClip.prototype.setCursor)) {
	trace("��� ������� �� ����������� ��� �������� _cursor ��� movieClip.");
}
/*MovieClip.prototype.check_cursor = function() {
	if (!paused || this.enable_on_pause) {
		//if (cursor_mc._currentframe != 3) {
			if (this.hitTest(_xmouse, _ymouse, true)) {
				//cursor_mc.gotoAndStop(this.cursor_id);
				if (this._visible) {
					cursor_mc.future_cursor = this.cursor_id;
				}
			}
		//}
		if (global_cursor_msg == "destroy") {
			this.setCursor(-1);
		}
	}
}*/
cursor_interval = setInterval(update_cursor, 150);
cursor_move_interval = setInterval(move_cursor, 15);
//cursor_mc.startDrag(true);
_global.cursor_registered_items = []
function move_cursor() {
	cursor_mc._x = _root._xmouse;
	cursor_mc._y = _root._ymouse;
}
function update_cursor() {
	/*if (paused && (cursor_mc._currentframe == 2)){
		cursor_mc.gotoAndStop(1);
	}else{*/
	cursor_mc.future_cursor = (_root._cursor) ? _root._cursor : 1;
	var i = cursor_registered_items.length;
	var found;
	while (i-- && !found) {
		if (cursor_registered_items[i] != undefined) {
			if (!paused || this.enable_on_pause) {
				//trace(cursor_registered_items[i]);
				if ((cursor_registered_items[i].cursor_framed_in == undefined) ||
					(cursor_registered_items[i].cursor_framed_in.hitTest(_xmouse, _ymouse))) {
					if (cursor_registered_items[i].hitTest(_root._xmouse, _root._ymouse, true)) {
						if (cursor_registered_items[i]._visible) {
							//trace(cursor_registered_items[i]);
							cursor_mc.future_cursor = cursor_registered_items[i].cursor_id;
							//trace(cursor_registered_items[i].cursor_id);
							found = true;
						}
					}
				}
				if (global_cursor_msg == "destroy") {
					cursor_registered_items[i].setCursor(-1);
				}
			}

		}
	}
		if (cursor_mc.future_cursor != cursor_mc._currentframe) {
			cursor_mc.gotoAndStop(cursor_mc.future_cursor);
		}
		if (cursor_mc.future_cursor == 0) {
			if (cursor_mc._alpha) {
				cursor_mc._alpha = 0;
			}
		} else {
			if (!cursor_mc._alpha) {
				cursor_mc._alpha = 100;
			}
		}
	//}
}
_global.flush_cursors = function() {
	/*var i = 0, j = cursor_registered_items.length;
	var cl = 0
	var res = []
	while (i < j) {
		if (cursor_registered_items[i]._cursor != undefined) {
			res.push(cursor_registered_items[i]);
		} else {
			cl++;
		}
		i++;
	}
	_global.cursor_registered_items = res;
	trace("cleaned " + cl);*/
	var i = cursor_registered_items.length
	while (i--) {
		cursor_registered_items[0]._cursor = -1
	}
	_global.cursor_registered_items = [];
}
_global.cursor_pause = function(flag) {
	if (flag) {
		if (cursor_mc._currentframe != 3) {
			cursor_mc.gotoAndStop(3);
		}
	} else {
		cursor_mc.gotoAndStop(1);
	}
}
function sort_by_depth(a, b) {
	if (a.getDepth() < b.getDepth()) {
		return -1;
	} else if (a.getDepth() > b.getDepth()) {
		return 1;
	} else {
		return 0;
	}
}
