/*
globals organizing by Bournazos Charilaos
do
	addGlobal(<string global name>, <value>);
instead of
	_global.<global name> = <value>;

addGlobal adds the global into the _global object but
it also adds it in an array. With this array as an index
you can erase all the global created in the current movie
by calling the clearMovieGlobals() function.
*/
//#include "./as/adds/array.as"
globals = [];
function addGlobal(name, value, custom_object){
	//if (globals.getPos(name) == undefined) {
		globals.push(name);
	//}
	_global[name] = value;
}

function clearMovieGlobals() {
	while (globals.length > 0) {
		delete _global[globals.pop()];
	}
}
