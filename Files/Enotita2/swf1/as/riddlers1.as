/*���������� ������ ����� 1:
  ���� �� ����������� - startDrag ���� ����� �������
  ������ ��������, ������ ��������� ������������ ���� ������ ��� ����.*/

function new_riddle(alim, agroups) {
update_ok = false
	_global.riddle = new Object();
	riddle.lastUp = _root.update_ok
	riddle.words = [];
	riddle.places = [];
	riddle.picked = undefined;
	riddle.refresh = undefined;
	riddle.limit = alim;
	riddle.found = 0;
	riddle.errcount = 0;
	riddle.groups = agroups;
	riddle.clear = function() {
		riddle.words = [];
		riddle.places = [];
		riddle.picked = undefined;
		riddle.refresh = undefined;
		riddle.limit = alim;
		riddle.found = 0;
		riddle.errcount = 0;
		riddle.groups = agroups;
	}
	riddle.mouse = new Object();
	riddle.mouse.onMouseUp = function() {
		if (!riddle.waiting) {
			switch (riddle.picked == undefined) {
			case true :
				var i = 0;
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip._alpha > 0) {
						if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
							riddle.picked = riddle.words[i];
							riddle.picked.clip.swapDepths(1000);
							riddle.refresh = setInterval(refresh_word, 5);
							sfx.play_sound(sfx.pick);
							break;
						}
					}
				}
				break;
			case false :
				last_try_clip_id = riddle.picked.id;
				var i = 0;
				for (i; i < riddle.words.length; i++) {
					if (riddle.words[i].clip._alpha > 0) {
						if (riddle.words[i].id != riddle.picked.id) {
							if (riddle.words[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
								riddle.picked.clip._x = riddle.picked.origin_x;
								riddle.picked.clip._y = riddle.picked.origin_y;
								riddle.picked = riddle.words[i];
								riddle.picked.clip.swapDepths(1000);
								riddle.refresh = setInterval(refresh_word, 5);
								sfx.play_sound(sfx.pick);
								break;
							}
						}
					}
				}
				var i = 0;
				var found = false;
				riddle.refresh = clearInterval(riddle.refresh);
				riddle.refresh = undefined;
				for (i; i < riddle.places.length; i++) {
					if (riddle.places[i].clip.hitTest(_root._xmouse, _root._ymouse, false)) {
						riddle.refresh = clearInterval(riddle.refresh);
						riddle.refresh = undefined;
						if (riddle.places[i].id == riddle.picked.id) {
							_root.stop_solution();
							riddle.places[i].clip._alpha = 100;
							riddle.picked.clip._alpha = 0;
							riddle.errcount = 0;
							sfx.play_sound(sfx.place_ok);
							if (++riddle.found == riddle.limit) {
								if (riddle.groups <= 1) {
									_root.riddle_done();
								} else {
									riddle.groups--;
									_root.group_solved();
								}
							}
							found = true
							_root.riddler.msave();
						} else {
							riddle.picked.clip._x = riddle.picked.origin_x;
							riddle.picked.clip._y = riddle.picked.origin_y;
							sfx.play_sound(sfx.place_wrong);
							if (++riddle.errcount == 3) {
								_root.give_help();
								riddle.errcount = 0;
							}
						}
						riddle.picked = undefined;
						break;
					}
				}
				if (!found) {
					riddle.picked.clip._x = riddle.picked.origin_x;
					riddle.picked.clip._y = riddle.picked.origin_y;
					riddle.picked = undefined;
					sfx.play_sound(sfx.place_wrong);
					if (++riddle.errcount == 3) {
						_root.give_help();
						riddle.errcount = 0;
					}
				}
				break;
			}
		}
	};
	Mouse.addListener(riddle.mouse);
}
function solve_riddle() {
	if (riddle.picked != undefined) {
		riddle.picked.clip._x = riddle.picked.origin_x;
		riddle.picked.clip._y = riddle.picked.origin_y;
		riddle.picked = undefined;
	}
	var i = 0;
	for (i; i < riddle.places.length; i++) {
		riddle.words[i].clip._alpha = 0;
		riddle.places[i].clip._alpha = 100;
		riddle.found++;
	}
	_root.riddle_done();
}
function refresh_word() {
	riddle.picked.clip._x = _xmouse;
	riddle.picked.clip._y = _ymouse;
	if (_root.update_OK) {
		updateAfterEvent();
	}

}
function add_word(aclip, aid) {
	var w = new item(aclip, aid);
	riddle.words.push(w);
	w.clip.useHandCursor = true;
	if (w.length == riddle.limit) {
		update_ok = riddle.lastUp;
	}
}
function add_place(aclip, aid) {
	var p = new item(aclip, aid);
	riddle.places.push(p);
	aclip._alpha = 0;
	if (riddle.places.length == riddle.limit) {
		_root.update_OK = true
	}
}
function item(aclip, aid) {
	this.clip = aclip;
	this.origin_x = aclip._x;
	this.origin_y = aclip._y;
	this.id = aid;
}