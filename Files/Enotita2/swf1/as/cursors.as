#include "as/generic.as"

Mouse.hide();
cursor = new Object();
cursor.initialize = function(aFlag) {
	cursor.shapeFlag = aFlag;
	cursor.clip = attachMovie("curs", "cursor_clip", 9990);
	cursor.clip._visible = false;
	cursor.registered_items = [];
	cursor.minX = 0
	cursor.minY = 0
	cursor.maxX = 640
	cursor.maxY = 480
	cursor.register_item = function(aClip, aType) {
		var itm = new Object();
		itm.clip = aClip;
		itm.type = aType;
		if (list_contains(this.registered_items, itm) == undefined) {
			this.registered_items.push(itm);
		}else{
			trace("duplicate");
		}
	};
	cursor.unregister_item = function(aClip) {
		var i = 0;
		var found = undefined;
		for (i; i < cursor.registered_items.length; i++) {
			if (cursor.registered_items[i].clip == aClip) {
				found = i;
				break;
			}
		}
		if (found != undefined) {
			if (cursor.registered_items.length != 0) {
				if (cursor.registered_items.length == 1) {
					cursor.registered_items = [];
				} else {
					if (found == cursor.registered_items.length - 1) {
						var tmp = cursor.registered_items.pop();
					} else {
						cursor.registered_items[found] = cursor.registered_items.pop();
					}
				}
			}
		}
	};
	cursor.unregister_type = function(aType) {
		var i = 0;
		for (i; i < cursor.registered_items.length; i++) {
			if (cursor.registered_items[i].type == aType) {
				if (cursor.registered_items.length != 0) {
					if (cursor.registered_items.length == 1) {
						cursor.registered_items = [];
					} else {
						if (i == cursor.registered_items.length - 1) {
							var tmp = cursor.registered_items.pop();
						} else {
							cursor.registered_items[i] = cursor.registered_items.pop();
						}
					}
				}	
			}
		}		
	};
};
cursor.clear_all = function() {
	this.registered_items = [];
}
cursor.update = function() {
	cursor.clip._x = _xmouse;
	cursor.clip._y = _ymouse;
	if ((_root._xmouse >= cursor.minX && _root._xmouse <= cursor.maxX) && (_root._ymouse >= cursor.minY && _root._ymouse <= cursor.maxY)) {
		if (_global.connector != undefined) {
			cursor.clip.gotoAndStop("iv");
		} else {
			if (cursor.waiting) {
				cursor.clip.gotoAndStop("w");
			} else {
				if (riddle.picked == undefined) {
					var i = 0;
					var found = undefined;
					for (i; i < cursor.registered_items.length; i++) {
						if (cursor.registered_items[i].clip._visible && cursor.registered_items[i].clip._alpha != 0){
							if (cursor.registered_items[i].clip.hitTest(_root._xmouse, _root._ymouse, cursor.shapeFlag)) {
								found = cursor.registered_items[i];
								break;
							}
						}
					}
					if (found == undefined) {
						cursor.clip.gotoAndStop("i");
					} else {
						switch (found.type) {
						case "button" :
							cursor.clip.gotoAndStop("ii");
							break;
						case "pickup" :
							cursor.clip.gotoAndStop("iii");
							break;
						case "text":
							cursor.clip.gotoAndStop("vii");
							break;
						case "paint":
							switch (_root.draw_mode) {
							case "brush":
								cursor.clip.gotoAndStop("v");
								_root.color_object(cursor.clip.brush.nose);
								break;
							case "rubber":
								cursor.clip.gotoAndStop("vi");
								break;
							}
							break;
						}
					}
				} else {
					cursor.clip.gotoAndStop("iv");
				}
				if (!cursor.clip._visible) {
					cursor.clip._visible = true;
				}
				if (_root.update_OK){
					updateAfterEvent()
				}
			}
		}
	} else {
		cursor.clip.gotoAndStop("i");
	}
};
cursor.updater = setInterval(cursor.update, 5);