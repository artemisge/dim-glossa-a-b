/*
################################
##	Bournazos Charilaos 2005  ##
################################
*/

/*
�� ���� �� ������ �������� ������� ���������
��� MovieClip ��� FlashMX
*/


//rotate to look at a given point

MovieClip.prototype.lookAt = function(ax, ay, interpolateBy, offsetRot) {
	var s1 = ay - this._y;
	var s2 = ax - this._x;
	var quat = [((s1 < 0) && (s2 < 0)) || ((s1 > 0) && (s2 < 0)), ((s1 < 0) && (s2 >= 0)) || ((s1 >= 0) && (s2 > 0))];
	var t = s1 / s2;
	var ang = ((360 * Math.atan(t) / Math.PI / 2) + offsetRot) / ((interpolateBy) ? interpolateBy : 1);

	if (quat[0]) {
		this._rotation = -(180 - ang);
	} else {
		this._rotation = (ang);
	}
};

/*
returns the projection of a given x on the y of the movieclip's content
from top to bottom
returns -1 if fails
*/

MovieClip.prototype.projectOnTop = function(aX) {
	var y = this._y - 1;
	var limY = y + this._height + 1;
	var failed = false;
	aX += this._x;
	while (!(this.hitTest(aX, y, true)) && !failed) {
		if ((y += 2) >= limY) {
			failed = true;
		}
	}
	return ((failed) ? -1 : y);
};
/*
_tint property implementation
changing the _tint value produces a bucket-fill like effect
*/
MovieClip.prototype.setTint = function(col) {
	this.__tint = col; //hold the value in a private var
	new Color(this).setRGB(this.__tint); //set the new color to me
}
MovieClip.prototype.getTint = function() {
	return this.__tint;  //return the color stored in the private var
}
//implement the propery
MovieClip.prototype.addProperty("_tint", this.getTint, this.setTint);

MovieClip.prototype.setAsButton = function(_var) {
	if (_var == "delete") {
		this.onRollOver = this.onRollOut = undefined;
	}else{
		this.gotoAndStop(1);
		this.onRelease = this.onRollOver = function() {
			this.gotoAndStop(2);
		}
		this.onRollOut = function() {
			this.gotoAndStop(1);
		}
		this.onPress = function() {
			this.gotoAndStop(3)
		}
	}
}

MovieClip.prototype.getIsButton = function() {
	return this.isMetaButton;
}


MovieClip.prototype.addProperty("_metaButton", this.getIsButton, this.setAsButton);

MovieClip.prototype.fit_into = function(mc, dCX, dCY) {
	var b1 = this.getBounds(_root);
	var b2 = mc.getBounds(_root);
	b2.xMin += dCX;
	b2.xMax -= dCX;
	b2.yMin += dCY;
	b2.yMax -= dCY;
	var nm1 = this._name;
	var nm2 = mc._name;
	var dx, dy;
	if (b1.xMin < b2.xMin) {
		dx = b2.xMin - b1.xMin;
	} else if (b1.xMax > b2.xMax) {
		dx = b2.xMax - b1.xMax;
	}
	if (b1.yMin < b2.yMin) {
		dy = b2.yMin - b1.yMin;
	} else if (b1.yMax > b2.yMax) {
		dy = b2.yMax - b1.yMax;
	}
	this._x += dx;
	this._y += dy;
};

