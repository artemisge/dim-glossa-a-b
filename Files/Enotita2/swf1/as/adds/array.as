/*
################################
##	Bournazos Charilaos 2005  ##
################################
*/

/*
�� ���� �� ������ �������� ������� ���������
��� Array ��� FlashMX
*/

Array.prototype.getPos = function(aElement) {
	var i = 0;
	for (i; i < this.length; i++) {
		if (this[i] == aElement) {
			if (debugging) {
				trace(i);
			}
			return i;
		}
	}
	if (debugging) {
		trace("no result...");
	}
};

Array.prototype.stick = function(aArray) {
	var i = 0
	for (i; i<aArray.length;i++){
		this.push(aArray[i]);
	}
}

Array.prototype.joinCopy = function(aArray) {
	/*res = [];
	var i = 0, j = this.length;
	while (i < j) {
		res[i] = this[i];
		i++;
	}
	i = 0, j = aArray.length;
	while (i < j) {
		res[i] = aArray[i];
		i++;
	}*/
	res = (this.join("\t") + "\t" + aArray.join("\t")).split("\t");
	return res;
}
Array.prototype.duplicate = function() {
	var res = []
	var i = 0
	var j = this.length;
	while (i < j) {
		if (this[i].length) {
			res[i] = this[i].duplicate();
		} else {
			res[i] = this[i];
		}
		i++;
	}
	return res;
}
Array.prototype.deleteAt = function(pos) {
	if (pos == 0) {
		this.shift();
	} else if (pos == (this.length - 1)) {
		this.pop();
	} else {
		this.splice(pos, 1);
	}
}
Array.prototype.suffle = function() {
	var res = [];
	var src = this;
	var r;
	while (src.length) {
		r = random(src.length);
		res.push(src[r]);
		src.deleteAt(r);
	}
	return res;
}
Array.prototype.setAll = function(val) {
	var i = 0, j = this.length;
	while (i < j) {
		if (typeof this[i] == "object") {
			this[i] = this[i].setAll(val);
		} else {
			this[i] = val;
		}
		i++;
	}
	return this;
};

Array.prototype.exists = function(val) {
	var i = 0, j = this.length;
	var res = false
	while ((i < j) && !res) {
		if (typeof this[i] == "object") {
			res = this[i].exists(val);
		} else {
			res = (this[i] == val);
		}
		i++;
	}
	return res;
}
