/*
################################
##	Bournazos Charilaos 2005  ##
################################
*/

/*
�� ���� �� ������ �������� ������� ���������
��� string ��� FlashMX
*/

/*
swaps old char <oc> with a new char <nc>
the length of the chars may vary and not equal to 1

possible usage for replacing while words or phrases (or chars ;))
*/
String.prototype.swap_char = function(oc, nc) {
	var i = 0;
	var res = this;
	while ((i = this.indexOf(oc, i)) > -1) {
		res = res.substring(0, i) + nc + res.substring(i + oc.length);
		i += nc.length
	}
	return res;
};

/*
extracts the extension (if there is one)
from the string. String remains on its original
form.
*/
String.prototype.get_extension = function() {
	var l = this.split(".");
	return (l.length > 1) ? l[l.length - 1] : false;
}

String.prototype.restrictEnter = function() {
	if (this.charAt(this.length-1) == "\r")
	{
		return this.substr(0, this.length-1);
	} else {
		return this;
	}
}
