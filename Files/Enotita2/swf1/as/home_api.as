_global.gui = function(flag) {
	buttons(flag);
	var i = 0
	for (i; i < gui_clips.length-1; i++) {
		gui_clips[i]._alpha = 100 - (60 * !flag);
	}
	//if (!(current_land > 0)) {
		_root.map._alpha = 100
	//}
}
_global.gui_init = function() {
	var i = 1
	for (i; i < gui_clips.length; i++) {
		_root.cursor.register_item(gui_clips[i], "button");
		_root.tooltip.register_item(gui_clips[i], gui_tips[i]);
		//trace(gui_tips[i]);
	}	
	clearInterval(_global.gui_refresh);
	delete _global.gui_refresh;
}
_global.buttons = function(flag) {
	var i = 0
	for (i; i < gui_clips.length-1; i++) {
		//gui_clips[i].gotoAndStop(1);
		gui_clips[i].trigger.enabled = flag;
		if (flag) {
			_root.cursor.register_item(gui_clips[i], "button");
		} else {
			_root.cursor.unregister_item(gui_clips[i]);
		}
	}
	//if (!(current_land > 0)) {
		_root.map.trigger.enabled = true
		_root.cursor.register_item(_root.map, "button");
	//}
}
_global.point = function(ax, ay) {
	this.x = ax;
	this.y = ay;
}
_global.distance = function(p1, p2) {
	var A = p1.x - p2.x;
	var B = p1.y - p2.y;
	var C = Math.sqrt(Math.pow(A, 2) + Math.pow(B, 2))
	return C
}
_global.book_open = function(){
	book_mc = _root.createEmptyMovieClip("big_book", 1002);
	book_mc._y = -10
	book_mc.loadMovie("theBook.swf");
	book.gotoAndStop(25);
}
_global.book_close= function(){
	book_mc.book_close();
	_root.book.trigger.enabled = false;
}
_global.clip_enable = function(aClip) {
	_root.cursor.register_item(aClip, "button")
	aClip.trigger.enabled = true;
	aClip._alpha = 100;
}
_global.clip_disable = function(aClip) {
	_root.cursor.unregister_item(aClip)
}
next = new Object();
next.clip = next_mc;
next.current_level = 0;
next.next_unit = "home_" + home_id + "_" + (unit_id + 1) + ".swf"
next.enable = function() {
	riddle.done = undefined;
	this.enabled = true;
	this.clip._alpha = 100;
	this.clip.trigger.enabled = true;
	//this.clip.gotoAndPlay(2);
	_root.cursor.register_item(this.clip, "button");
	_root.tooltip.register_item(this.clip, "�������");
};
next.disable = function() {
	_root.cursor.unregister_item(this.clip);
	_root.tooltip.unregister_item(this.clip);
	this.enabled = false;
	this.clip.gotoAndStop(1);
	this.clip._alpha = 40;
	this.clip.trigger.enabled = false;
};
next.execute = function(reversed) {
	clearInterval(_root.help.heady.timer);
	_root.update_ok = false;
	sfx.play_sound(sfx.pick);
	if (next.current_level == next.last_level) {
		_root.gotoMovie(this.next_unit);
	} else {
		this.current_level = Math.max(0, Math.min((reversed) ? this.current_level - 1 : this.current_level + 1, this.last_level));
		_root.reset_riddle()
		if (riddle.picked != undefined) {
			removeMovieClip(riddle.picked.clip);
		}
		var i = 0;
		for (i; i < riddle.places.length; i++) {
			if ((riddle.taken[riddle.places[i].place] == 0) || !check_doubles) {
				riddle.taken[riddle.places[i].place]++
				riddle.places[i].clip._alpha = 100;
			}
		}
		var i = 0;
		for (i; i < riddle.words.length; i++) {
			_root.cursor.unregister_item(riddle.words[i].clip);
		}
		Mouse.removeListener(riddle.mouse);
		riddle.found = riddle.limit;
		riddle.picked = undefined;
		clearInterval(riddle.refresh);
		riddle.refresh = undefined;
		_root.update_ok = false;
		_global.current_movie = "riddle_" + home_id + "_" + unit_id + "_" + this.current_level + ".swf";
		_root.riddler.loadMovie("riddle_" + home_id + "_" + unit_id + "_" + this.current_level + ".swf");
		_root.lisi.gotoAndStop(1);
		//trace(this.current_level +" "+ this.last_level+" "+ this.next_unit)
		//this.disable();
		if (this.current_level < this.last_level || this.next_unit != undefined) {
			_root.cursor.register_item(next.clip, "button")
			this.enable();
		} else {
			this.disable();
			_root.cursor.unregister_item(next.clip)
		}
		if (this.current_level > 0) {
			_root.cursor.register_item(prev_mc, "button")
		} else {
			_root.cursor.unregister_item(prev_mc)
		}
	}
};
function riddle_done(check_doubles, noSound) {
	riddle.done = true;	
	sfx.stop_sound();
	if (riddle.picked != undefined) {
		removeMovieClip(riddle.picked.clip);
	}
	var i = 0;
	for (i; i < riddle.places.length; i++) {
		if ((riddle.taken[riddle.places[i].place] == 0) || !check_doubles) {
			riddle.taken[riddle.places[i].place]++
			riddle.places[i].clip._alpha = 100;
		}
	}
	var i = 0;
	for (i; i < riddle.words.length; i++) {
		_root.cursor.unregister_item(riddle.words[i].clip);
	}
	Mouse.removeListener(riddle.mouse);
	riddle.found = riddle.limit;
	riddle.picked = undefined;
	clearInterval(riddle.refresh);
	riddle.refresh = undefined;
	if (gen_time == undefined) {
		gen_time = setInterval(riddle_done, 20, check_doubles);
	} else {
		_root.riddler.msave();
		if (last_sound == undefined) {
			if (solved_sounds[next.current_level - 1] != undefined) {
				last_sound = new Sound();
				last_sound.attachSound(solved_sounds[next.current_level - 1]);
				last_sound.onSoundComplete = function() {
					after_last_sound();
				}
			} else {
				after_last_sound();
			}
			last_sound.start();
		}
	}
	//this is a way to execute code in the movie when i want it at the end of some riddles.
	end_of_riddle();
}
function after_last_sound() {
	if (next.current_level == next.last_level) {
		sfx.play_sound(sfx.well_done);
	} else {
		sfx.play_sound(sfx.good[random(sfx.good.length)]);
	}
	var done = (riddler.found == riddler.limit);
	if (done) {
		_root.lisi.unregister();
		clearInterval(gen_time);
		gen_time = undefined;
		var i = 0;
		if ((next.current_level == next.last_level) && (next.next_unit == undefined)) {
			_root.map.gotoAndPlay(2);
		} else {
			next.enable();
			next.clip.gotoAndPlay(3);
		}
	}
	delete last_sound;
}
function initialize_tabs() {
	var i = 1;
	for (i; i <= 4; i++) {
		cursor.register_item(eval("t" + i), "button");
	}
}
function intersects(clipA, clipB, offset) {
	var b1 = clipA.getBounds(_root);
	var b2 = clipB.getBounds(_root);
	var fin = false;
	if (((b1.xMin > b2.xMin - offset) && (b1.xMin < b2.xMax + offset)) || ((b1.xMax > b2.xMin - offset) && (b1.xMax < b2.xMax + offset))) {
		if (((b1.yMin > b2.yMin - offset) && (b1.yMin < b2.yMax + offset)) || ((b1.yMax > b2.yMin - offset) && (b1.yMax < b2.yMax + offset))) {
			fin = true;
		}
	}
	return fin;
}
function ugh_delay(aVal) {
	var i = 0;
	for (i; i < aVal; i++) {
	
	}
}
function gotoMovie(aMovie) {
	_root.update_OK = false;
	_global.current_movie = aMovie;
	unloadMovie(_root)
	flush();	
	loadMovie(aMovie, _root)	
}
function flush() {
	Mouse.removeListener(riddle.mouse);
	riddle.found = riddle.limit;
	delete riddle.picked;
	delete riddle;
	delete _global.riddle;
}
function back_to_map() {
	_root.gotoMovie("Xartis.swf");
}
_global. home_to_num = function(){
	var res;
	switch (home_id) {
		case "A":
			res = 1;
			break;
		case "B":
			res = 2;
			break;
		case "C":
			res = 3;
			break;
		case "D":
			res = 4;
			break;
		case "E":
			res = 5;
			break;
		}
	return res
}
gui_clips = [_root.help, _root.lex, _root.enc, _root.book, _root.map, next.clip];
_global.gui_tips = ["�������", "�� ������ ��� ���������� ��� ������ ������.", "� �������������� ��� ���������� ��� ������ ������.", "�������!", "������!", "�������!"];
_global.gui_refresh = setInterval(gui_init, 250);
solved_sounds = ["win", "win", "win", "win", "win", "win"];
